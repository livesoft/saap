package com.iteye.dwangel.util;

import org.mvel2.templates.TemplateRuntime;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Implement evaluate values according to context, before put();
 * Created by Simon Xianyu on 2014/9/19.
 */
public class EvaluteProperties extends Properties {

    private Map<String, Object>  context = new HashMap<String, Object>();

    @Override
    public synchronized Object put(Object key, Object value) {
        if (key == null) {
            throw new IllegalArgumentException("Key should not be null");
        }
        Object resultValue;
        if (value != null) {
            String valueText = value == null?"":value.toString();
            resultValue = TemplateRuntime.eval(valueText, this.context);
        } else {
            resultValue = "";
        }
        this.context.put(key.toString(), resultValue);
        return super.put(key, resultValue);
    }
}
