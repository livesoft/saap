package com.iteye.dwangel.util.spring;

import com.iteye.dwangel.util.EvaluteProperties;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.Properties;

/**
 * FactoryBean for EvaluteProperties.
 * Created by Administrator on 2014/9/19.
 */
public class EvalutePropertiesFactoryBean extends PropertiesFactoryBean {

    @Override
    protected Properties mergeProperties() throws IOException {
        EvaluteProperties result = new EvaluteProperties();
        if (this.localOverride) {
            // Load properties from file upfront, to let local properties override.
            loadProperties(result);
        }

        if (this.localProperties != null) {
            for (Properties localProp : this.localProperties) {
                CollectionUtils.mergePropertiesIntoMap(localProp, result);
            }
        }

        if (!this.localOverride) {
            // Load properties from file afterwards, to let those properties override.
            loadProperties(result);
        }

        return result;
    }
}
