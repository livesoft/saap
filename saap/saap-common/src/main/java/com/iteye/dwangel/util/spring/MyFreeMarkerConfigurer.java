package com.iteye.dwangel.util.spring;

import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.IOException;
import java.util.Map;

/**
 * Share some method object.
 * Created by simon on 2015/2/5.
 */
public class MyFreeMarkerConfigurer extends FreeMarkerConfigurer{
    private Map<String, Object> modelMap;

    @Override
    public void afterPropertiesSet() throws IOException, TemplateException {
        super.afterPropertiesSet();
        if (modelMap != null){
            for(Map.Entry<String, Object> entry : modelMap.entrySet()) {
                this.getConfiguration().setSharedVariable(entry.getKey(), entry.getValue());
            }
        }
    }

    public Map<String, Object> getModelMap() {
        return modelMap;
    }

    public void setModelMap(Map<String, Object> modelMap) {
        this.modelMap = modelMap;
    }
}
