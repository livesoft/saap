package com.iteye.dwangel.util.spring;

import org.springframework.web.servlet.view.freemarker.FreeMarkerView;

import java.io.IOException;
import java.util.Locale;

/**
 * Override super class to check internal resource existence.
 * Created by simon on 2015/1/27.
 */
public class LayoutFreemarkerView extends FreeMarkerView {
    private String contentUrl;
    @Override
    public boolean checkResource(Locale locale) throws Exception {
        if (contentUrl!=null) {
            try {
                getTemplate(contentUrl, locale);
            } catch (IOException e) {
//                e.printStackTrace();
                return false;
            }
        }

        return super.checkResource(locale);
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }
}
