package com.iteye.dwangel.util;

/**
 * Define this class to specify some values in configuration.
 * Created by simon on 2015/1/30.
 */
public abstract class BaseWebConfig {
    /** External resource url */
    protected String extResUrl;

    public String getExtResUrl() {
        return extResUrl;
    }

    public void setExtResUrl(String extResUrl) {
        this.extResUrl = extResUrl;
    }
}
