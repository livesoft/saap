package com.litt.saap.system.vo;

import java.io.Serializable;


/**
 * 租户成员信息.
 * 
 * <pre><b>描述：</b>
 *    用户所在租户中的成员信息
 * </pre>
 * 
 * <pre><b>修改记录：</b>
 *    
 * </pre>
 * 
 * @author <a href="mailto:littcai@hotmail.com">蔡源</a>
 * @since 2015年2月1日
 * @version 1.0
 */
public class TenantMemberVo implements Serializable {
  
  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** 租户ID. */
  private int tenantId;
  
  /** 是否租户管理员. */
  private boolean isAdmin;
  
  /** 所在租户的成员类型. */
  private int memberType;
  
  /** 所在租户的成员状态. */
  private int memberStatus;   

  /**
   * Gets the tenant id.
   *
   * @return the tenantId
   */
  public int getTenantId() {
    return tenantId;
  }

  /**
   * Sets the tenant id.
   *
   * @param tenantId the tenantId to set
   */
  public void setTenantId(int tenantId) {
    this.tenantId = tenantId;
  }

  /**
   * Gets the checks if is admin.
   *
   * @return the isAdmin
   */
  public boolean getIsAdmin() {
    return isAdmin;
  }

  /**
   * Sets the checks if is admin.
   *
   * @param isAdmin the isAdmin to set
   */
  public void setIsAdmin(boolean isAdmin) {
    this.isAdmin = isAdmin;
  }

  /**
   * @return the memberStatus
   */
  public int getMemberStatus() {
    return memberStatus;
  }

  /**
   * @param memberStatus the memberStatus to set
   */
  public void setMemberStatus(int memberStatus) {
    this.memberStatus = memberStatus;
  }
  
  /**
   * @return the memberType
   */
  public int getMemberType()
  {
    return memberType;
  }
  
  /**
   * @param memberType the memberType to set
   */
  public void setMemberType(int memberType)
  {
    this.memberType = memberType;
  } 

}
