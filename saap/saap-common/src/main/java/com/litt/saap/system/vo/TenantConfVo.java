package com.litt.saap.system.vo;

import java.io.Serializable;

public class TenantConfVo implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /**
   * 租户ID.
   */
  private int tenantId;

  private CompanyInfo companyInfo = new CompanyInfo();

  private MailReceiverInfo mailReceiverInfo = new MailReceiverInfo();

  private MailSenderInfo mailSenderInfo = new MailSenderInfo();

  private SmsServerInfo smsServerInfo = new SmsServerInfo();

  public TenantConfVo()
  {
  }

  public TenantConfVo(int tenantId)
  {
    this.tenantId = tenantId;
  }

  /**
   * Gets the tenant id.
   *
   * @return the tenant id
   */
  public int getTenantId()
  {
    return tenantId;
  }
  
  /**
   * @param tenantId the tenantId to set
   */
  public void setTenantId(int tenantId)
  {
    this.tenantId = tenantId;
  }

  public class CompanyInfo {
    private String name;
    private String shortName;
    private String intro;
    private String contract;
    private String telephone;
    private String fax;
    private String email;
    private String im;
    private String zipcode;
    private String address;
    private String billingAddress;
    private String shippingAddress;
    
    public String getName()
    {
      return name;
    }
    
    public void setName(String name)
    {
      this.name = name;
    }
    
    public String getShortName()
    {
      return shortName;
    }
    
    public void setShortName(String shortName)
    {
      this.shortName = shortName;
    }
    
    public String getIntro()
    {
      return intro;
    }
    
    public void setIntro(String intro)
    {
      this.intro = intro;
    }
    
    public String getContract()
    {
      return contract;
    }
    
    public void setContract(String contract)
    {
      this.contract = contract;
    }
    
    public String getTelephone()
    {
      return telephone;
    }
    
    public void setTelephone(String telephone)
    {
      this.telephone = telephone;
    }
    
    public String getFax()
    {
      return fax;
    }
    
    public void setFax(String fax)
    {
      this.fax = fax;
    }
    
    public String getEmail()
    {
      return email;
    }
    
    public void setEmail(String email)
    {
      this.email = email;
    }
    
    public String getIm()
    {
      return im;
    }
    
    public void setIm(String im)
    {
      this.im = im;
    }
    
    public String getZipcode()
    {
      return zipcode;
    }
    
    public void setZipcode(String zipcode)
    {
      this.zipcode = zipcode;
    }
    
    public String getAddress()
    {
      return address;
    }
    
    public void setAddress(String address)
    {
      this.address = address;
    }

    
    public String getBillingAddress()
    {
      return billingAddress;
    }

    
    public void setBillingAddress(String billingAddress)
    {
      this.billingAddress = billingAddress;
    }

    
    public String getShippingAddress()
    {
      return shippingAddress;
    }

    
    public void setShippingAddress(String shippingAddress)
    {
      this.shippingAddress = shippingAddress;
    }
    
    
  }

  public class MailReceiverInfo {
    private boolean enabled;
    private String protocol;
    private String host;
    private int port;    
    private boolean sslEnabled;
    private int sslPort;
    private String account;
    private String password;
    
    public String getProtocol()
    {
      return protocol;
    }
    
    public void setProtocol(String protocol)
    {
      this.protocol = protocol;
    }
    
    public String getHost()
    {
      return host;
    }
    
    public void setHost(String host)
    {
      this.host = host;
    }
    
    public int getPort()
    {
      return port;
    }
    
    public void setPort(int port)
    {
      this.port = port;
    }
    
    public boolean isSslEnabled()
    {
      return sslEnabled;
    }
    
    public void setSslEnabled(boolean sslEnabled)
    {
      this.sslEnabled = sslEnabled;
    }
    
    public int getSslPort()
    {
      return sslPort;
    }
    
    public void setSslPort(int sslPort)
    {
      this.sslPort = sslPort;
    }
    
    public String getAccount()
    {
      return account;
    }
    
    public void setAccount(String account)
    {
      this.account = account;
    }
    
    public String getPassword()
    {
      return password;
    }
    
    public void setPassword(String password)
    {
      this.password = password;
    }

    
    public boolean isEnabled()
    {
      return enabled;
    }

    
    public void setEnabled(boolean enabled)
    {
      this.enabled = enabled;
    }
  }

  public class MailSenderInfo {
    private boolean enabled;
    private String host;
    private int port;
    private boolean sslEnabled;
    private int sslPort;
    private String encoding;
    private String account;
    private String password;
    private boolean signatureEnabled;
    
    public String getHost()
    {
      return host;
    }
    
    public void setHost(String host)
    {
      this.host = host;
    }
    
    public int getPort()
    {
      return port;
    }
    
    public void setPort(int port)
    {
      this.port = port;
    }
    
    public boolean isSslEnabled()
    {
      return sslEnabled;
    }
    
    public void setSslEnabled(boolean sslEnabled)
    {
      this.sslEnabled = sslEnabled;
    }
    
    public int getSslPort()
    {
      return sslPort;
    }
    
    public void setSslPort(int sslPort)
    {
      this.sslPort = sslPort;
    }
    
    public String getEncoding()
    {
      return encoding;
    }
    
    public void setEncoding(String encoding)
    {
      this.encoding = encoding;
    }
    
    public String getAccount()
    {
      return account;
    }
    
    public void setAccount(String account)
    {
      this.account = account;
    }
    
    public String getPassword()
    {
      return password;
    }
    
    public void setPassword(String password)
    {
      this.password = password;
    }
    
    public boolean isSignatureEnabled()
    {
      return signatureEnabled;
    }
    
    public void setSignatureEnabled(boolean signatureEnabled)
    {
      this.signatureEnabled = signatureEnabled;
    }

    
    public boolean isEnabled()
    {
      return enabled;
    }

    
    public void setEnabled(boolean enabled)
    {
      this.enabled = enabled;
    }
  }

  public class SmsServerInfo {
    private boolean enabled;
    private String protocol;
    private String host;
    private String account;
    private String password;
    
    public String getProtocol()
    {
      return protocol;
    }
    
    public void setProtocol(String protocol)
    {
      this.protocol = protocol;
    }
    
    public String getHost()
    {
      return host;
    }
    
    public void setHost(String host)
    {
      this.host = host;
    }
    
    public String getAccount()
    {
      return account;
    }
    
    public void setAccount(String account)
    {
      this.account = account;
    }
    
    public String getPassword()
    {
      return password;
    }
    
    public void setPassword(String password)
    {
      this.password = password;
    }

    
    public boolean isEnabled()
    {
      return enabled;
    }

    
    public void setEnabled(boolean enabled)
    {
      this.enabled = enabled;
    }
  }

  
  /**
   * @return the companyInfo
   */
  public CompanyInfo getCompanyInfo()
  {
    return companyInfo;
  }

  
  /**
   * @param companyInfo the companyInfo to set
   */
  public void setCompanyInfo(CompanyInfo companyInfo)
  {
    this.companyInfo = companyInfo;
  }

  
  /**
   * @return the mailReceiverInfo
   */
  public MailReceiverInfo getMailReceiverInfo()
  {
    return mailReceiverInfo;
  }

  
  /**
   * @param mailReceiverInfo the mailReceiverInfo to set
   */
  public void setMailReceiverInfo(MailReceiverInfo mailReceiverInfo)
  {
    this.mailReceiverInfo = mailReceiverInfo;
  }

  
  /**
   * @return the mailSenderInfo
   */
  public MailSenderInfo getMailSenderInfo()
  {
    return mailSenderInfo;
  }

  
  /**
   * @param mailSenderInfo the mailSenderInfo to set
   */
  public void setMailSenderInfo(MailSenderInfo mailSenderInfo)
  {
    this.mailSenderInfo = mailSenderInfo;
  }

  
  /**
   * @return the smsServerInfo
   */
  public SmsServerInfo getSmsServerInfo()
  {
    return smsServerInfo;
  }

  
  /**
   * @param smsServerInfo the smsServerInfo to set
   */
  public void setSmsServerInfo(SmsServerInfo smsServerInfo)
  {
    this.smsServerInfo = smsServerInfo;
  }
}
