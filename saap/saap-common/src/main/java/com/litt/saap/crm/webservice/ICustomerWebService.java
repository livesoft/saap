package com.litt.saap.crm.webservice;

import java.util.List;

import com.litt.saap.crm.vo.CustomerVo;


/**
 * ICustomerWebService.
 * 
 * <pre><b>Descr:</b>
 *    
 * </pre>
 * 
 * <pre><b>Changelog:</b>
 *    
 * </pre>
 * 
 * @author <a href="mailto:littcai@hotmail.com">Caiyuan</a>
 * @since 2014年10月8日
 * @version 1.0
 */
public interface ICustomerWebService {
  
  /**
   * Find.
   *
   * @param id the id
   * @return the customer vo
   */
  public CustomerVo find(Integer id);  
  
  public CustomerVo findByName(int tenantId,String customerName);
  
  public List<CustomerVo> findBy(int tenantId, String code, String name, int[] customerIds) ;

}
