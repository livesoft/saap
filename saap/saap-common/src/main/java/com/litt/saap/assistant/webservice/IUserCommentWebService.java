package com.litt.saap.assistant.webservice;

import com.litt.core.dao.page.IPageList;
import com.litt.core.dao.ql.PageParam;
import com.litt.saap.assistant.vo.UserCommentVo;

/**
 * .
 * 
 * <pre><b>描述：</b>
 *    
 * </pre>
 * 
 * <pre><b>修改记录：</b>
 *    
 * </pre>
 * 
 * @author <a href="mailto:littcai@hotmail.com">蔡源</a>
 * @since 2015年4月28日
 * @version 1.0
 */
public interface IUserCommentWebService {

  public UserCommentVo save(int tenantId, int userId, String moduleCode, int dataId, String content);

  /**
   * Delete by id.
   * @param id id
   */
  public void delete(Integer id);

  /**
   * Find page.
   *
   * @param pageParam the page param
   * @return the i page list
   */
  public IPageList findPage(PageParam pageParam);

}