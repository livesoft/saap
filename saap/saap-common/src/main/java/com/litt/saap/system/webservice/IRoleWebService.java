package com.litt.saap.system.webservice;

import java.util.List;

import com.litt.saap.system.vo.RoleVo;



/**
 * @author cwang
 */
public interface IRoleWebService {

	public List<RoleVo> listByUser(int userId);

}