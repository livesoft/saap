package com.litt.saap.assistant.vo;

import java.io.Serializable;
import java.util.Date;

public class UserCommentVo implements Serializable {
	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 序号.
	 */
	private Integer id;

	/**
	 * 租户ID.
	 */
	private int tenantId;

	/**
	 * 模块编号.
	 */
	private String moduleCode;

	/**
	 * 数据ID.
	 */
	private int recordId;

	/**
	 * 内容.
	 */
	private String content;

	/**
	 * 创建人.
	 */
	private int createBy;
	
	/** The create username. */
	private String createUsername;

	/**
	 * 创建时间.
	 */
	private Date createDatetime;
	/*
	 * **************************************
	 *           附加属性
	 * **************************************
	 */
	private String createUserAvatar;
	

	public UserCommentVo() {
	}

	public UserCommentVo(int tenantId, String moduleCode, int dataId,
			String content, int createBy, Date createDatetime) {
		this.tenantId = tenantId;
		this.moduleCode = moduleCode;
		this.recordId = dataId;
		this.content = content;
		this.createBy = createBy;
		this.createDatetime = createDatetime;
	}

	/**  
	 * Get 序号.
	 * @return 序号
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * Set 序号.
	 * @param id 序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**  
	 * Get 租户ID.
	 * @return 租户ID
	 */
	public int getTenantId() {
		return this.tenantId;
	}

	/**
	 * Set 租户ID.
	 * @param tenantId 租户ID
	 */
	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	/**  
	 * Get 模块编号.
	 * @return 模块编号
	 */
	public String getModuleCode() {
		return this.moduleCode;
	}

	/**
	 * Set 模块编号.
	 * @param moduleCode 模块编号
	 */
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	/**  
	 * Get 数据ID.
	 * @return 数据ID
	 */
	public int getRecordId() {
		return this.recordId;
	}

	/**
	 * Set 数据ID.
	 * @param dataId 数据ID
	 */
	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}

	/**  
	 * Get 内容.
	 * @return 内容
	 */
	public String getContent() {
		return this.content;
	}

	/**
	 * Set 内容.
	 * @param content 内容
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**  
	 * Get 创建人.
	 * @return 创建人
	 */
	public int getCreateBy() {
		return this.createBy;
	}

	/**
	 * Set 创建人.
	 * @param createBy 创建人
	 */
	public void setCreateBy(int createBy) {
		this.createBy = createBy;
	}

	/**  
	 * Get 创建时间.
	 * @return 创建时间
	 */
	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	/**
	 * Set 创建时间.
	 * @param createDatetime 创建时间
	 */
	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}
  
  public String getCreateUsername()
  {
    return createUsername;
  }
  
  public void setCreateUsername(String createUsername)
  {
    this.createUsername = createUsername;
  }

  
  public String getCreateUserAvatar()
  {
    return createUserAvatar;
  }

  
  public void setCreateUserAvatar(String createUserAvatar)
  {
    this.createUserAvatar = createUserAvatar;
  }

}
