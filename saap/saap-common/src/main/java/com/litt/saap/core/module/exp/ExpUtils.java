package com.litt.saap.core.module.exp;

import java.io.File;
import java.io.InputStream;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.litt.core.web.util.WebUtils;


/**
 * .
 * 
 * <pre><b>描述：</b>
 *    
 * </pre>
 * 
 * <pre><b>修改记录：</b>
 *    
 * </pre>
 * 
 * @author <a href="mailto:littcai@hotmail.com">蔡源</a>
 * @since 2015年3月2日
 * @version 1.0
 */
public class ExpUtils {
  
  /**
   * Exp.
   *
   * @param response the response
   * @param type 导出类型
   * @param templateName 模板名称
   * @param templatePath 模板路径
   * @param targetName 目标文件爱你名
   * @param dataMap 数据映射
   * @throws Exception the exception
   */
  public static void exp(HttpServletResponse response, String type, String homePath, String templateName, String templatePath, String targetName, Map<String, Object> dataMap) throws Exception
  {    
    String ext = StringUtils.substringAfterLast(templateName, ".");
    String targetPath = homePath + File.separator + "tmp";
    String targetFileName = targetName+"."+ext;
    File targetFile = null;
    if("excel".equals(type))
    {
      InputStream templateInputStream = ExpUtils.class.getResourceAsStream(templatePath+templateName);
      
      //ExcelExpWriter writer = new ExcelExpWriter(targetPath, targetFileName);
      //writer.setDataList(dataList);
      
      ExcelExtExpWriter writer  = new ExcelExtExpWriter(targetPath, targetFileName);
      writer.addData(dataMap);
      
      targetFile = writer.build(templateInputStream);
    }   
    else if("word".equals(type))
    {
      InputStream templateInputStream = ExpUtils.class.getResourceAsStream(templatePath+templateName);      
      WordExpWriter writer = new WordExpWriter(targetPath, targetFileName);
      //WordXmlExpWriter writer = new WordXmlExpWriter(targetPath, targetFileName);
      writer.addData(dataMap);
      
      targetFile = writer.build(templateName, templateInputStream);
    }
    WebUtils.download(response, targetFileName, targetFile);
  }

}
