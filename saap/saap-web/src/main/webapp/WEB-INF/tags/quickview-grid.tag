<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.littcore.com/core" prefix="li" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ attribute name="table" required="true" rtexprvalue="true" type="com.litt.saap.core.module.quickview.model.table.Table" %>
<%@ attribute name="rsList" required="false" rtexprvalue="true" type="java.util.Collection" %>
<%@ attribute name="var" required="true" rtexprvalue="false" %>
<%@ variable name-from-attribute="var" alias="v" scope="NESTED" variable-class="java.lang.Object" %>
<table class="table table-striped table-bordered table-hover datatable" style="width: 100%">
<thead>
  <tr>
  <c:forEach items="${table.columnList }" var="column">
    <th class="${column.hide?'hide':'' } ${'checkbox' eq column.format?'checkCol':'' }  column_${column.name }  ${column.sortable?' sortable':' ' }"  data-column="${column.sortField }" data-order="${column.sortField eq param.sortField?param.sortOrder:'' }" >
      <c:choose>  
        <c:when test="${'checkbox' eq column.format }">
          <input type="checkbox" id="checkAll" name="checkAll" />
        </c:when>
        <c:otherwise><s:message code="${column.title }" /></c:otherwise>
      </c:choose>
    </th> 
  </c:forEach>
  <th><s:message code="common.action" /></th>
  </tr>
</thead>
<tbody>
  <c:forEach items="${requestScope[table.dataset].rsList }" var="row" varStatus="rowStatus">
      <c:set var="v" value="${row }" />
                    <tr>
                      <c:forEach items="${table.columnList }" var="column">
                      <td class="${column.hide?'hide':'' } ${'checkbox' eq column.format?'checkCol':'' } column_${column.name }">
                        <c:choose>
                          <c:when test="${'checkbox' eq column.format }">
                            <input type="checkbox" class="checkItem" name="primaryIds" value="${row[column.name] }" />
                          </c:when>           
                          <c:when test="${'date' eq column.dataType}">
                            <fmt:formatDate value="${row[column.name]}" pattern="${column.format }"/>
                          </c:when>
                          <c:when test="${'decimal' eq column.dataType}">
                            <fmt:formatNumber value="${row[column.name]}" pattern="${column.format }"/>
                          </c:when>                                           
                          <c:otherwise><c:out value="${row[column.name]}"></c:out></c:otherwise>
                        </c:choose>   
                      </td>
                      </c:forEach>
                      <td>                      
                        <jsp:doBody></jsp:doBody>
                      </td>
                    </tr> 
  </c:forEach>
</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){ 
  tableSort(".datatable", "${param.sortField}", "${param.sortOrder}");
});  

function tableSort(targetTable, sortField, sortOrder)
{
	$(targetTable+" > thead > tr > th.sortable").each(function(){	 
	 
	  var $this = $(this);
	  //$this.addClass("sort-both");
	  var columnName = $this.attr("data-column");
	  var columnOrder = $this.attr("data-order");
	  if(columnName!=null && columnName==sortField)
	  {
	    $this.addClass("sort-"+sortOrder);
	  }
	  else
	  {
	    $this.addClass("sort-both");
	  } 
	  
      if("asc"==columnOrder)
        columnOrder = "desc";
      else
        columnOrder = "asc";
	  $this.click(function(){
	    var url = location.href; 
	    if(url.indexOf("sortField")>0)
	  	{
	  		url = url.replaceAll("sortField="+sortField, "sortField="+columnName);
	  	}
	  	else if(url.indexOf("?")<0)
	  	{
	  		url = url + "?sortField=" + columnName;
	  	}
	  	else
	  	{
	  		url = url + "&sortField=" + columnName;
	  	}
	    //
	    if(url.indexOf("sortOrder")>0)
	  	{
	  		url = url.replaceAll("sortOrder="+sortOrder, "sortOrder="+columnOrder);
	  	}
	  	else if(url.indexOf("?")<0)
	  	{
	  		url = url + "?sortOrder=" + columnOrder;
	  	}
	  	else
	  	{
	  		url = url + "&sortOrder=" + columnOrder;
	  	}
	    location.href = url;
	    //$("#sortField").val(column); 
	    //$("#sortOrder").val(order); 
	    //$("#searchform").submit();
	  });
	});  
}
</script>    