<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/common.jspf"%>
<%@ include file="/common/taglibs.jspf"%>
<html lang="en">
  <head>
	  <style>
		  ul.perm {clear:both; list-style: none;}
		  ul.perm > li {clear:both;}
	  </style>
  </head>
  <body>   
  <form id="theform" action="update.json" method="post" class="form-horizontal">
  	<input type="hidden" name="id" value="${role.id}" />
		<fieldset class="collapsible">
			<legend><s:message code="common.ui.fieldset.base" /></legend>
			<div class="row-fluid">
				<div class="control-group">
					<label class="control-label" for="name"><s:message code="role.name" /></label>
					<div class="controls">
						<input id="name" name="name" placeholder="" type="text" value="<c:out value='${role.name}' />" />
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="control-group">
					<label class="control-label" for="remark"><s:message code="role.remark" /></label>
					<div class="controls">
						<textarea rows="6" cols="8" id="remark" name="remark" class="input-block-level limited"><c:out value='${role.remark}' /></textarea>
					</div>
				</div>
			</div>
		</fieldset>
		<fieldset class="collapsible">
			<legend><s:message code="role.ui.fieldset.permission" /></legend>
			<!-- datatable -->
			<div>
				<div id="form-error-box"></div>

				<c:forEach items="${permissionTree.subList }" var="domain">
				<fieldset class="collapsible">
					<legend>
						<input type="checkbox" id="domain${domain.idCode }" name="permissionCodes" value="${domain.code }"  <c:if test="${domain.checked }">checked="checked"</c:if>/>
						<s:message code="domain.${domain.code }" />
					</legend>
					<ul class="perm">
					<c:forEach items="${domain.subList }" var="level2">
						<li>
						<c:if test="${level2.type == 2 }">
						<label class="l_block_head">
							<input type="checkbox" class="checkItem domain${domain.idCode }" id="module${level2.idCode }" name="permissionCodes" value="${level2.code }"
								   <c:if test="${level2.checked }">checked="checked"</c:if> />&nbsp;<s:message code="module.${level2.code }" />
						</label>

						<ul>
							<c:forEach items="${level2.subList }" var="func">
							<li class="l_f_block"><label >
								<input type="checkbox" class="checkItem  domain${domain.idCode } module${level2.idCode }" name="permissionCodes" value="${func.code }"
									   <c:if test="${func.checked }">checked="checked"</c:if> />&nbsp;<s:message code="func.${func.code }" />
							</label></li>
							</c:forEach>
						</ul>
						</c:if>
						</li>
					</c:forEach>
					</ul>

				</fieldset>
				</c:forEach>

			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary"><i class="icon-ok"></i> <s:message code="btn.save" /></button>
			<button type="button" class="btn" onclick="history.back();"><s:message code="btn.cancel" /></button>
		</div>
				
    </form>
		<!--page specific plugin scripts-->				
		<script type="text/javascript">
		$(document).ready(function(){
			
			<c:forEach items="${permissionTree.subList }" var="domain">
			{				
				<c:forEach items="${domain.subList }" var="level2">
					<c:if test="${level2.type == 2 }">
					{
						var code = "module"+"${level2.idCode}"; 
						$.webtools.checkboxs({
							checkAll: "#"+code,
							checkItem: "."+code				
						});
					}
					</c:if>
					<c:if test="${level2.type == 1 }">						
						<c:forEach items="${level2.subList }" var="level3">
						{
							var code = "module"+"${level3.idCode}"; 
							$.webtools.checkboxs({
								checkAll: "#"+code,
								checkItem: "."+code				
							});
						}
						</c:forEach>
						
						{
							var code = "domain"+"${level2.idCode}"; 
							$.webtools.checkboxs({
								checkAll: "#"+code,
								checkItem: "."+code				
							});
						}
					</c:if>	
				</c:forEach>
				
				var code = "domain"+"${domain.idCode}"; 
				var checkboxs = $.webtools.checkboxs({
					checkAll: "#"+code,
					checkItem: "."+code				
				});
			}
			</c:forEach>
			
			var checkboxs = $.webtools.checkboxs({
				checkAll: "#checkAll",
				checkItem: ".checkItem"				
			});
			
			$('#theform').littFormSubmit({
				rules : {
					name : {
						required : true,
						maxlength: 50
					}
				},	
				beforeSubmit: function(){
					if($(".checkItem:checked").length<=0)
					{
						$.webtools.alert({
			    			containerId: "form-error-box",
							type: "error",
							message: "<s:message code='validate.checkone'/>"			
				 		}); 						
						return false;
					}
				},
				success: function(reply){
					 location.href = <h:returnUrl value="index.do" />;
				}
			});
		});
		</script>	  
  </body>	
</html>