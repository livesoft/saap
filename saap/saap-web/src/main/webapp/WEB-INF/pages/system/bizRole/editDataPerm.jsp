<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/common.jspf"%>
<%@ include file="/common/taglibs.jspf"%>
<html lang="en">
<head>
</head>
<body>
	<form id="theform" action="updateDataPerm.json" method="post"
		class="form-horizontal">
		<input type="hidden" id="bizRoleId" name="id" value="${bizRole.id}" />
		<input type="hidden" id="dataType" name="dataType" value="chargeBy" />
		<fieldset class="collapsible">
			<legend>
				<s:message code="common.ui.fieldset.base" />
			</legend>
			<div class="row-fluid">
				<div class="control-group">
					<label class="control-label" for="name"><s:message
							code="bizRole.name" /></label>
					<div class="controls">
						<input id="name" name="name" placeholder="" type="text" readonly="readonly"
							value="<c:out value='${bizRole.name}' />" />
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="control-group">
					<label class="control-label" for="remark"><s:message code="role.remark" /></label>
					<div class="controls">
						<textarea rows="6" cols="8" id="remark" name="remark" readonly="readonly" class="input-block-level limited"><c:out value='${bizRole.remark}' />
						</textarea>
					</div>
				</div>
			</div>
		</fieldset>
		<fieldset class="collapsible">
			<legend>
				<%-- <s:message code="userGroup" /> --%>
				数据权限
			</legend>
			
			<ul class="nav nav-tabs">
		  		<li class="active"><a href="#chargeBy" data-toggle="tab" onclick="selectTab('chargeBy');">系统用户</a></li>
		  		<li class=""><a href="#customerId" data-toggle="tab" onclick="selectTab('customerId');">客户</a></li>
			</ul>
			
			<div class="tab-content">
				<div class="tab-panel active" id="chargeBy">
					<table class="table table-striped table-bordered table-hover datatable">
						<thead>
							<tr>
								<th class="checkCol"><input type="checkbox" id="checkAll" name="checkAll" /></th>
								<th><s:message code="userInfo.loginId" /></th>
								<th><s:message code="userInfo.userName" /></th>
								<th><s:message code="userInfo.nickName" /></th>
								<th><s:message code="userInfo.gender" /></th>
								<th><s:message code="userInfo.status" /></th>
								<!-- <th>权限访问字段名</th> -->
								<th><s:message code="tenantMember.createDatetime" /></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${pageList.rsList }" var="row">
								<tr class="editDataPerm">
									<td class="checkCol"><input type="checkbox"
										class="checkItem" name="targetIds"
										value="${row.userInfo.id }"
										<c:if test="${row.checked }">checked="checked"</c:if> /></td>
									<td><c:out value="${row.userInfo.loginId }"></c:out></td>
									<td><c:out value="${row.userInfo.userName }"></c:out></td>
									<td><c:out value="${row.userInfo.nickName }"></c:out></td>
									<td><c:out value="${li:genDictContent('0002', row.userInfo.gender) }"></c:out></td>
									<td><c:out value="${li:genDictContent('1002', row.userInfo.status) }"></c:out></td>
									<%-- <td>
										<input name="fieldNames" placeholder="" type="text" value="<c:if test="${row.checked }">${row.fieldName }</c:if>" />
									</td> --%>
									<td><c:out value="${li:formatDateTime(row.userInfo.createDatetime) }"></c:out></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="tab-panel" id="customerId">
					<table class="table table-striped table-bordered table-hover datatable">
						<thead>
							<tr>			
								<th class="checkCol"><input type="checkbox" id="checkAll" name="checkAll" /></th>	
								<th class="sort code"><s:message code="customer.code" /></th>	
								<th class="sort name"><s:message code="customer.name" /></th>
								<th><s:message code="customer.contacts" /></th>	
								<th><s:message code="customer.phone" /></th>					
								<th><s:message code="customer.email" /></th>
								<th><s:message code="customer.chargeUser" /></th>						
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${pageList2.rsList }" var="row">
							<tr class="editDataPerm1">
								<td class="checkCol">
									<input type="checkbox" class="checkItem" name="targetIds" value="${row.ID }" <c:if test="${row.checked }">checked="checked"</c:if>/>
								</td>
								<td><c:out value="${row.CODE }"></c:out></td>
								<td><c:out value="${row.NAME }"></c:out></td>
								<td><c:out value="${row.CONTACTS_NAME }"></c:out></td>
								<td><c:out value="${row.PHONE }"></c:out></td>
								<td><a href="mailto:${row.EMAIL }">${row.EMAIL }</a></td>
								<td><c:out value="${row.CHARGE_USER_NAME }"></c:out></td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="button" class="btn btn-primary" onclick="editDataPerm(this);">
				<i class="icon-ok"></i>
				<s:message code="btn.save" />
			</button>
			<button type="button" class="btn" onclick="history.back();">
				<s:message code="btn.cancel" />
			</button>
		</div>

	</form>
	<!--page specific plugin scripts-->
	<script type="text/javascript">
		function selectTab(key){
			if(key == 'chargeBy'){
				$("#customerId").hide();
				$("#chargeBy").show();
				$("#dataType").val("chargeBy");
			}
			
			else if(key == 'customerId'){
				$("#chargeBy").hide();
				$("#customerId").show();
				$("#dataType").val("customerId");
			}
		}	
	
		$(document).ready(function(){
			$("#customerId").hide();
			
			<c:forEach items="${nodeList }" var="module">
				{
					var code = "module" + "${module.moduleCode}";
					var checkboxs = $.webtools.checkboxs({
						checkAll : "#" + code,
						checkItem : "." + code
					});
				}
			</c:forEach>

			var checkboxs = $.webtools.checkboxs({
				checkAll : "#checkAll",
				checkItem : ".checkItem"
			});

			/* $('#theform1').littFormSubmit({
				rules : {
					name : {
						required : true,
						maxlength : 50
					}
				}, */
				/* beforeSubmit: function(){
					if($(".checkItem:checked").length<=0)
					{
						$.webtools.alert({
							containerId: "form-error-box",
							type: "error",
							message: "<s:message code='validate.checkone'/>"			
				 		}); 						
						return false;
					}
				}, */
				/* success : function(reply) {
					location.href = <h:returnUrl value="index.do"></h:returnUrl>;
				}
			}); */
		});
		
		function editDataPerm(obj){
			var str = "";
			//var isSubmit = true;
			var dataType = $("#dataType").val();
			if(dataType == 'chargeBy'){
				$(".editDataPerm").each(function(){
					var tdChilds = $(this).children();
					var checked = $(tdChilds[0]).children()[0].checked;
					var targetId = $(tdChilds[0]).children()[0].value;
					if(checked){
						if(str == ""){
							str += targetId;
						}
						
						else{
							str += ";";
							str += targetId;
						}
					}
				});
			}
			else if(dataType == 'customerId'){
				$(".editDataPerm1").each(function(){
					var tdChilds = $(this).children();
					var checked = $(tdChilds[0]).children()[0].checked;
					var targetId = $(tdChilds[0]).children()[0].value;
					if(checked){
						if(str == ""){
							str += targetId;
						}
						
						else{
							str += ";";
							str += targetId;
						}
					}
				});
			}
			
			if(str != ''){
				var id = $("#bizRoleId").val();
				$.ajax({
			     	type: "POST",
			     	data: {
			     		data: str,
			     		id: id,
			     		fieldName: dataType
			     	},
			     	url: "updateDataPerm.json",
			     	success: function(result){
			     		location.href = <h:returnUrl value="index.do"></h:returnUrl>;
			     	}
				});
			}
		}
	</script>
</body>
</html>