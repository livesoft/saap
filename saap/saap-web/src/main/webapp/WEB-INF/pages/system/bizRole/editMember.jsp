<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/common.jspf"%>
<%@ include file="/common/taglibs.jspf"%>
<html lang="en">
  <head>		
  </head>
  <body>   
  <form id="theform" action="updateMember.json" method="post" class="form-horizontal">
  		<input type="hidden" name="id" value="${bizRole.id}" />
		<fieldset class="collapsible">
			<legend><s:message code="common.ui.fieldset.base" /></legend>
			<div class="row-fluid">
				<div class="control-group">
					<label class="control-label" for="name"><s:message code="bizRole.name" /></label>
					<div class="controls">
						<input id="name" name="name" placeholder="" type="text" readonly="readonly" value="<c:out value='${bizRole.name}' />" />
					</div>
				</div>						
			</div>
			<div class="row-fluid">
				<div class="control-group">
					<label class="control-label" for="remark"><s:message code="role.remark" /></label>
					<div class="controls">
						<textarea rows="6" cols="8" id="remark" name="remark" readonly="readonly" class="input-block-level limited"><c:out value='${bizRole.remark}' /></textarea>
					</div>
				</div>								
			</div>
		</fieldset>						
		<fieldset class="collapsible">
			<legend>
				<%-- <s:message code="userGroup" /> --%>
				角色成员
			</legend>
			<table class="table table-striped table-bordered table-hover datatable">				
				<thead>
				  <tr>
					<th class="checkCol"><input type="checkbox" id="checkAll" name="checkAll" /></th>	
					<th><s:message code="userInfo.loginId" /></th>
					<th><s:message code="userInfo.userName" /></th>
					<th><s:message code="userInfo.nickName" /></th>
					<th><s:message code="userInfo.gender" /></th>
					<th><s:message code="userInfo.status" /></th>
					<th><s:message code="tenantMember.createDatetime" /></th>
					<th><s:message code="tenantMember.updateDatetime" /></th>
				  </tr>	
				</thead>
				<tbody>
				  <c:forEach items="${pageList.rsList }" var="row">
					<tr>
						<td class="checkCol"><input type="checkbox" class="checkItem" name="roleMemberIds" value="${row.userInfo.id }" <c:if test="${row.checked }">checked="checked"</c:if>/></td>
						<td><c:out value="${row.userInfo.loginId }"></c:out></td>
						<td><c:out value="${row.userInfo.userName }"></c:out></td>
						<td><c:out value="${row.userInfo.nickName }"></c:out></td>
						<td><c:out value="${li:genDictContent('0002', row.userInfo.gender) }"></c:out></td>
						<td><c:out value="${li:genDictContent('1002', row.userInfo.status) }"></c:out></td>
						<td><c:out value="${li:formatDateTime(row.userInfo.createDatetime) }"></c:out></td>
						<td><c:out value="${li:formatDateTime(row.userInfo.updateDatetime) }"></c:out></td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</fieldset>
						
		<div class="form-actions">			
			<button type="submit" class="btn btn-primary"><i class="icon-ok"></i> <s:message code="btn.save" /></button>		
			<button type="button" class="btn" onclick="history.back();"><s:message code="btn.cancel" /></button>
		</div>					
				
	</form>			
	<!--page specific plugin scripts-->				
		<script type="text/javascript">
		$(document).ready(function(){	
			
		  var checkboxs = $.webtools.checkboxs({
				checkAll: "#checkAll",
				checkItem: ".checkItem"				
			});
		  
			$('#theform').littFormSubmit({
				rules : {					
					roleMemberIds : {
						required : true
					}
				},			
				success: function(reply){
					 location.href = <h:returnUrl value="index.do"></h:returnUrl>;					
				}
			});
		});
		</script>		  
  </body>	
</html>