<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ include file="/common/common.jspf"%>
<%@ include file="/common/taglibs.jspf"%>
<html lang="en">
  <head>		
  </head>
  <body>   
  <form id="theform" action="save.json" method="post" class="form-horizontal">
				<fieldset class="collapsible">
					<legend><s:message code="common.ui.fieldset.base" /></legend>
					<div class="row-fluid">
						<div class="control-group">
							<label class="control-label" for="name"><s:message code="role.name" /></label>
							<div class="controls">
								<input id="role.name" name="name" placeholder="" type="text" />
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="control-group">
							<label class="control-label" for="remark"><s:message code="role.remark" /></label>
							<div class="controls">
								<textarea rows="6" cols="8" id="remark" name="remark" class="input-block-level limited"></textarea>
							</div>
						</div>								
					</div>
				</fieldset>					
				<fieldset class="collapsible">
					<legend><s:message code="role.ui.fieldset.permission" /></legend>
					<!-- datatable -->
					<div>
						<div id="form-error-box"></div>
						<table class="table table-bordered table-hover datatable">
							<thead>
								<tr>
									<td colspan="2"><input type="checkbox" class="checkAll" id="checkAll" name="checkAll"/>&nbsp;<s:message code="common.ui.selectAll" /></td>	
								</tr>
							</thead>							
							<tbody>
							<c:forEach items="${nodeList }" var="module">
								<tr>
									<td class="checkCol"><input type="checkbox" class="checkItem" id="module${module.moduleCode }" name="moduleCodes" value="${module.moduleCode }" /></td>						
									<td><s:message code="module.${module.moduleCode }"></s:message></td>									
								</tr>
								<tr>
									<td>&nbsp;</td>							
									<td>
										<c:forEach items="${module.fields }" var="field">											
											<input type="checkbox" class="checkItem module${module.moduleCode }" name="fields" value="${module.moduleCode }-${field.field }" />&nbsp;<s:message code="field.${module.moduleCode }.${field.field }"></s:message>
										</c:forEach>
									</td>									
								</tr>
							</c:forEach>
							</tbody>
						</table>						
					</div> 
				</fieldset>		
						
				<div class="form-actions">
					<button type="submit" class="btn btn-primary"><i class="icon-ok"></i> <s:message code="btn.save" /></button>
					<button type="button" class="btn" onclick="history.back();"><s:message code="btn.cancel" /></button>
				</div>					
				
			</form>				
		<!--page specific plugin scripts-->				
		<script type="text/javascript">
		$(document).ready(function(){	
						
			<c:forEach items="${nodeList }" var="module">
			{				
				var code = "module"+"${module.moduleCode}"; 
				var checkboxs = $.webtools.checkboxs({
					checkAll: "#"+code,
					checkItem: "."+code				
				});
			}
			</c:forEach>
			
			var checkboxs = $.webtools.checkboxs({
				checkAll: "#checkAll",
				checkItem: ".checkItem"				
			});
			
			$('#theform').littFormSubmit({
				rules : {
					name : {
						required : true,
						maxlength: 50
					}
				},	
				/* beforeSubmit: function(){
					if($(".checkItem:checked").length<=0)
					{
						$.webtools.alert({
			    			containerId: "form-error-box",
							type: "error",
							message: "<s:message code='validate.checkone'/>"			
				 		}); 						
						return false;
					}
				}, */
				success: function(reply){
					 location.href =  <h:returnUrl value="index.do"></h:returnUrl>;					
				}
			});
		});
		
		</script>	  
  </body>	
</html>