<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/common.jspf"%>
<%@ include file="/common/taglibs.jspf"%>
<html lang="en">
<head>
  <meta name="decorator" content="popup" />  
  <!-- angularjs -->
  <script type="text/javascript" src="${contextPath }/theme/default/js/angular.js"></script>  
</head>
<body>
  <!-- feedback -->
      <div class="row-fluid" ng-controller="FeedbackCtrl">
        <div class="widget-box">
        <div class="widget-header header-color-blue">
          <h5 class="bigger lighter"><s:message code="common.message.info" /></h5>
        </div>
        <div class="widget-body">
            <div class="widget-main">
                <table class="table table-striped table-hover table-condensed" style="width: 100%">
                  <tr ng-repeat="feedback in feedbackList">
                    <td>
                      <span class="label label-info">{{feedback.typeDescr }}</span>&nbsp;&nbsp;<a href="javascript:;" ng-click="showFeedback(feedback)">{{feedback.content}}</a>
                
                      <div class="hide" id="feedbackReplyBox{{feedback.id}}">
                        <span class="badge pull-right">{{feedback.replyDatetime | date : 'yyyy-MM-dd hh:mm:ss' }}</span>
                        <span class="label label-success"><s:message code="feedback.reply" /></span>
                        {{feedback.reply}}
                      </div>
                    </td>
                    <td><span class="badge">{{feedback.createDatetime | date : 'yyyy-MM-dd hh:mm:ss' }}</span></td>
                  </tr>
                </table>  
             </div>
          </div>
        
        </div>
      </div>

    <div class="center">
      <button type="button" class="btn" onclick="window.opener=null;window.open('','_self');window.close();"> <s:message code="btn.close" /></button>
    </div>
  <script type="text/javascript">
  $(document).ready(function(){  
    
    angular.element(document).ready(function() {	
  		//angular.bootstrap(document);	
  		// Your app's root module...
      angular.module('MyModule', []).config(function($httpProvider) {
        // Use x-www-form-urlencoded Content-Type
        $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded';
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        /**
         * The workhorse; converts an object to x-www-form-urlencoded serialization.
         * @param {Object} obj
         * @return {String}
         */ 
        var param = function(obj) {
          var query = '', name, value, fullSubName, subName, subValue, innerObj, i;
            
          for(name in obj) {
            value = obj[name];
              
            if(value instanceof Array) {
              for(i=0; i<value.length; ++i) {
                subValue = value[i];
                fullSubName = name + '[' + i + ']';
                innerObj = {};
                innerObj[fullSubName] = subValue;
                query += param(innerObj) + '&';
              }
            }
            else if(value instanceof Object) {
              for(subName in value) {
                subValue = value[subName];
                fullSubName = name + '[' + subName + ']';
                innerObj = {};
                innerObj[fullSubName] = subValue;
                query += param(innerObj) + '&';
              }
            }
            else if(value !== undefined && value !== null)
              query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
          }
            
          return query.length ? query.substr(0, query.length - 1) : query;
        };

        // Override $http service's default transformRequest
        $httpProvider.defaults.transformRequest = [function(data) {
          return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
        }];
      });
  	
      angular.bootstrap(document, ['MyModule']);	
  	});
  });
  
  function FeedbackCtrl($scope, $http, $filter) {
    $scope.feedbackList = [];
    var pageIndex = 2;
    
    $http.post("${contextPath}/message/feedback/getTopN.json").success(function(data) {
      var feedbackList = data.feedbackList;
      for(i=0;i<feedbackList.length;i++)
      {
        var feedback = feedbackList[i];
        if(1==feedback.type)
        {
          feedback.typeDescr = "<s:message code='feedback.type.1' />"; 
        }
        else if(2==feedback.type)
        {
          feedback.typeDescr = "<s:message code='feedback.type.2' />"; 
        }
        else if(3==feedback.type)
        {
          feedback.typeDescr = "<s:message code='feedback.type.3' />"; 
        }
        else if(4==feedback.type)
        {
          feedback.typeDescr = "<s:message code='feedback.type.4' />"; 
        }
      }
      $scope.feedbackList = feedbackList;
    });   
    
    $scope.showMoreFeedback = function(){
      $http.post("${contextPath}/message/feedback/getTopN.json", {"pageIndex": pageIndex}).success(function(data) {
        var feedbackList = data.feedbackList;
        if(0==feedbackList.length)
        {
          $.webtools.notify({
            type: "notice",
            position: "center",
            delay: 1500,
            message: "<s:message code='btn.more.nomore' />"			
          }); 
          return;
        }
        for(i=0;i<feedbackList.length;i++)
        {
          var feedback = feedbackList[i];
          if(1==feedback.type)
          {
            feedback.typeDescr = "<s:message code='feedback.type.1' />"; 
          }
          else if(2==feedback.type)
          {
            feedback.typeDescr = "<s:message code='feedback.type.2' />"; 
          }
          else if(3==feedback.type)
          {
            feedback.typeDescr = "<s:message code='feedback.type.3' />"; 
          }
          else if(4==feedback.type)
          {
            feedback.typeDescr = "<s:message code='feedback.type.4' />"; 
          }
          $scope.feedbackList.push(feedback);
        }
        pageIndex++;
      });   
    }
    
    $scope.showFeedback = function(feedback) {
      $("#feedbackReplyBox"+feedback.id).toggle();
    }
  }
  </script>
</body>
</html>