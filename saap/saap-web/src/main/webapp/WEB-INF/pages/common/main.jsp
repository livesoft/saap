<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/common.jspf"%>
<%@ include file="/common/taglibs.jspf"%>
<html lang="en" ng-app="MyModule">
	<head>
      <!-- portal -->
      <link href="${contextPath}/static/widgets/portal/css/inettuts.css" rel="stylesheet" type="text/css" />
      <link href="${contextPath}/static/widgets/portal/css/inettuts.ext.css" rel="stylesheet" type="text/css" />
      <script type="text/javascript" src="${contextPath}/static/widgets/jquery-ui/js/jquery-ui-1.10.0.custom.min.js"></script>
      <script type="text/javascript" src="${contextPath}/static/widgets/portal/js/inettuts.js"></script>
      <!-- angularjs -->
      <script type="text/javascript" src="${contextPath }/theme/default/js/angular.js"></script>	
      <style>
        .list-table a {color: black;}
        .more-div {text-align: center;padding: 5px;margin: 3px 0;cursor: pointer;}
        .more-div span {font-size: 12px;}
        .more-div:hover {background-color: #f5f5f5;}
      </style>
	</head>
	<body>	
      <div class="row-fluid">       
        <div id="columns">
            <ul id="column1" class="column">
                <li class="widget color-blue">
                  <div class="widget-head">
                      <h3>我的任务</h3>
                  </div>
                  <div id="myTaskWidget" class="widget-content">
                    <table class="table table-striped table-hover table-condensed" style="width: 100%">
                      <thead>
                        <tr>
                          <th width="35px">序号</th>
                          <th width="200px">任务名称</th>
                          <th width="100px">所属项目</th>
                        </tr>
                      </thead>  
                      <tbody>  
                        <tr>
                          <td>1</td>
                          <td>完成销售、备料、生产、发货完整流程</td>
                          <td>赛蒙</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>开发请购单和采购模块</td>
                          <td>赛蒙</td>
                        </tr>
                      </tbody>
                    </table>
                    <div class="more-div" onclick="showDocMore(1);" type="1">
                      <span>更多</span> <i class="icon-chevron-down icon-large"></i>
                    </div>
                  </div>
                </li>  
                <li class="widget color-blue">
                    <div class="widget-head">
                        <h3>我的待办</h3>
                    </div>
                    <div id="myTodoWidget" class="widget-content" ng-controller="TodoCtrl">
                      <table class="table table-striped table-hover table-condensed" style="width: 100%">
                        <col width="24"/>
                        <tbody>
                          <tr ng-repeat="todo in todoList">
                            <td><input type="checkbox" ng-click="changeStatusDone($index, todo.status);" /></td>
                            <td><label id="todo_{{todo.id}}">{{todo.content}}</label></td>
                          </tr>
                        </tbody>
                      </table>
                      <div class="more-div" ng-click="showMore();"><span>更多</span> <i class="icon-chevron-down icon-large"></i></div>
                    </div>
                </li>               
                <li class="widget color-blue">  
                    <div class="widget-head">
                        <h3>常用联系人</h3>
                    </div>
                    <div id="starDocWidget" class="widget-content">
                      <p class="empty">无</p>
                    </div>
                </li>
            </ul>
    
            <ul id="column2" class="column">
                <li class="widget color-blue">  
                    <div class="widget-head">
                        <h3>本周日程计划</h3>
                    </div>
                    <div id="starDocWidget" class="widget-content">
                      <p class="empty">无</p>
                    </div>
                </li>
                <li class="widget color-blue">  
                    <div class="widget-head">
                        <h3>最近打开的文档</h3>
                    </div>
                    <div id="opDocWidget" class="widget-content">
                      <p class="empty">无</p>
                    </div>
                </li>
                <li class="widget color-blue">  
                    <div class="widget-head">
                        <h3>我收藏的文档</h3>
                    </div>
                    <div id="starDocWidget" class="widget-content">
                      <p class="empty">无</p>
                    </div>
                </li>
                
            </ul>
            
            <ul id="column3" class="column">
                <li class="widget color-blue">  
                    <div class="widget-head">
                        <h3><s:message code="affiche"></s:message></h3>
                    </div>
                    <div id="affichePanel" class="widget-content" ng-controller="AfficheCtrl">
                          <table class="table table-striped table-hover table-condensed" style="width: 100%">
                              <tbody>
                                <tr ng-repeat="affiche in afficheList">
                                  <td>
                                    <span class="label label-info">{{affiche.typeDescr }}</span>&nbsp;&nbsp;<a href="${contextPath }/message/affiche/show.do?id={{affiche.id}}" target="_blank">{{affiche.title}}</a>
                                  </td>
                                  <td><small class="muted pull-right">[{{affiche.updateDatetime | date : 'yyyy-MM-dd' }}]</small></td>
                                </tr>
                              </tbody>
                          </table> 
                          <div class="more-div" ng-click="showMoreAffiche()"><span>更多</span> <i class="icon-chevron-down icon-large"></i></div>
                        </div>
                </li>
            </ul>
        </div>
      </div>
  
      
      
      <!-- inline scripts related to this page -->
      <script type="text/javascript">
      $(document).ready(function(){    
        iNettuts.init();
        
        angular.element(document).ready(function() {	
      		//angular.bootstrap(document);	
      		// Your app's root module...
          angular.module('MyModule', []).config(function($httpProvider) {
            // Use x-www-form-urlencoded Content-Type
            $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded';
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
            /**
             * The workhorse; converts an object to x-www-form-urlencoded serialization.
             * @param {Object} obj
             * @return {String}
             */ 
            var param = function(obj) {
              var query = '', name, value, fullSubName, subName, subValue, innerObj, i;
                
              for(name in obj) {
                value = obj[name];
                  
                if(value instanceof Array) {
                  for(i=0; i<value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                  }
                }
                else if(value instanceof Object) {
                  for(subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                  }
                }
                else if(value !== undefined && value !== null)
                  query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
              }
                
              return query.length ? query.substr(0, query.length - 1) : query;
            };

            // Override $http service's default transformRequest
            $httpProvider.defaults.transformRequest = [function(data) {
              return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
            }];
          });
      	
          angular.bootstrap(document, ['MyModule']);	
      	});
      });
      
      function AfficheCtrl($scope, $http, $filter) {
        $scope.afficheList = [];
        var pageIndex = 2;
        
        $http.post("${contextPath}/message/affiche/getTopN.json").success(function(data) {
          var afficheList = data.afficheList;
          for(i=0;i<afficheList.length;i++)
          {
            var affiche = afficheList[i];
            if(1==affiche.type)
            {
              affiche.typeDescr = "<s:message code='dictparam.affiche-type.1' />"; 
            }
            else if(2==affiche.type)
            {
              affiche.typeDescr = "<s:message code='dictparam.affiche-type.2' />"; 
            }
            else if(3==affiche.type)
            {
              affiche.typeDescr = "<s:message code='dictparam.affiche-type.3' />"; 
            }
          }
          $scope.afficheList = afficheList;
        });
        
        $scope.showMoreAffiche = function(){
          $http.post("${contextPath}/message/affiche/getTopN.json", {"pageIndex": pageIndex}).success(function(data) {
            var afficheList = data.afficheList;
            if(0==afficheList.length)
            {
              $.webtools.notify({
                type: "notice",
                position: "center",
                delay: 1500,
                message: "<s:message code='btn.more.nomore' />"			
              }); 
              return;
            }
            for(i=0;i<afficheList.length;i++)
            {
              var affiche = afficheList[i];
              if(1==affiche.type)
              {
                affiche.typeDescr = "<s:message code='dictparam.affiche-type.1' />"; 
              }
              else if(2==affiche.type)
              {
                affiche.typeDescr = "<s:message code='dictparam.affiche-type.2' />"; 
              }
              else if(3==affiche.type)
              {
                affiche.typeDescr = "<s:message code='dictparam.affiche-type.3' />"; 
              }
              $scope.afficheList.push(affiche);
            }
            pageIndex++;
          });
        }        
      }
      
      function TodoCtrl($scope, $http, $filter) {
        $scope.todoList = [];
        var pageIndex = 2;
        
        $http.post("${contextPath}/personal/todo/getTopN.json").success(function(data) {
          var todoList = data.todoList;          
          $scope.todoList = todoList;
        });
        
        $scope.changeStatusDone = function(index, status){    
          var todo = $scope.todoList[index];
          if(status!=3)
            status=3;
          else
            status=2;
          $http.post("${contextPath}/personal/todo/updateStatus.json", {"id": todo.id, "status":status}).success(function(data) {
            todo.status = status;
            if(status==3)
            {
              $("#todo_"+todo.id).addClass("f-deleted");
            }
            else
            {
              $("#todo_"+todo.id).removeClass("f-deleted");
            }
          });         
        }
        
        $scope.showMore = function(){
          $http.post("${contextPath}/personal/todo/getTopN.json", {"pageIndex": pageIndex}).success(function(data) {
            var todoList = data.todoList;
            if(0==todoList.length)
            {
              $.webtools.notify({
                type: "notice",
                position: "center",
                delay: 1500,
                message: "<s:message code='btn.more.nomore' />"			
              }); 
              return;
            }
            $scope.todoList.push(todoList);
            pageIndex++;
          });
        }        
      }
      </script>
	</body>
</html>
