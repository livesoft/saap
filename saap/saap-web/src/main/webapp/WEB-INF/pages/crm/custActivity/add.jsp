<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/common.jspf"%>
<%@ include file="/common/taglibs.jspf"%>
<html lang="en">
<head>
<!-- bootstrap-datepicker -->
<link href="${contextPath}/static/widgets/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<script type="text/javascript" src="${contextPath}/static/widgets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<c:if test="${!empty SESSION_USER && !empty SESSION_USER.locale && !fn:startsWith(SESSION_USER.locale, 'en') }">
	<script src="${contextPath }/static/widgets/bootstrap-datepicker/js/locales/bootstrap-datepicker.${SESSION_USER.locale }.js"></script>
</c:if>
</head>
<body>
	<form id="theform" action="save.json" method="post" class="form-horizontal">
		<fieldset>
			<legend>
				<s:message code="common.ui.fieldset.base" />
			</legend>
            <div class="row-fluid">
              <div class="control-group">
                <label class="control-label" for="customerId"><s:message code="custActivity.customerId" /></label>
                <div class="controls">
                  <input type="hidden" id="customerId" name="customerId" class="span5" />
                </div>
              </div>
            </div>
            <div class="row-fluid">
              <div class="control-group">
                <label class="control-label" for="contactId"><s:message code="custActivity.contactId" /></label>
                <div class="controls">
                  <select id="contactId" name="contactId"></select>
                </div>
              </div>
            </div>
			<div class="row-fluid">
				<div class="control-group">
					<label class="control-label" for="actType"><s:message code="custActivity.actType" /></label>
					<div class="controls">
						<select id="actType" name="actType" data-placeholder="<s:message code='common.ui.select' />">
							<li:dictOptions dictType="custActivity-actType"/>
						</select>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="control-group">
					<label class="control-label" for="subject"><s:message code="custActivity.subject" /></label>
					<div class="controls">
						<input id="subject" name="subject" placeholder="" type="text" />
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="control-group">
					<label class="control-label" for="content"><s:message code="custActivity.content" /></label>
					<div class="controls">
						<textarea rows="5" cols="8" id="content" name="content" class="input-block-level limited"></textarea>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="control-group">
					<label class="control-label" for="actDate"><s:message code="custActivity.actDate" /></label>
					<div class="controls">
						<div class="input-append date datepicker" data-date-format="yyyy-mm-dd">
							<input id="actDate" name="actDate" type="text" value="${li:formatDate(now) }" /> <span class="add-on"><i class="icon-calendar"></i></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="control-group">
					<label class="control-label" for="nextActDate"><s:message code="custActivity.nextActDate" /></label>
					<div class="controls">
						<div class="input-append date datepicker" data-date-format="yyyy-mm-dd">
							<input id="nextActDate" name="nextActDate" placeholder="" type="text" value="" /> <span class="add-on"><i class="icon-calendar"></i></span>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row-fluid">
				<div class="control-group">
					<label class="control-label" for="chargeBy"><s:message code="custActivity.chargeBy" /></label>
					<div class="controls">
						<select id="chargeBy" name="chargeBy" data-placeholder="<s:message code='common.ui.select' />">
							<option value=""></option>
							<li:optionsCollection collection="${chargeUserList }" var="row">
								<li:option property="${row.id }" selected="${row.id == currentUserId }">${row.userName } (${row.loginId})</li:option>
							</li:optionsCollection>
						</select>
					</div>
				</div>
			</div>
		</fieldset>

		<div class="form-actions">
			<button type="submit" class="btn btn-primary">
				<i class="icon-ok"></i>
				<s:message code="btn.save" />
			</button>
			<button type="button" class="btn" onclick="history.back();">
				<s:message code="btn.cancel" />
			</button>
		</div>

	</form>
	<!--page specific plugin scripts-->
	<script type="text/javascript">
		$(document).ready(function() {

			$('.datepicker').datepicker({
				todayBtn : true,
				todayHighlight : true
			});

			$('#theform').littFormSubmit({
				rules : {
					customerId : {
						required : true
					},
					subject : {
						required : true
					},
					chargeBy : {
						required : true
					}
				},
				success : function(reply) {
					location.href = <h:returnUrl value="index.do"></h:returnUrl>;
				}
			});
		});

		$("#customerId").select2({
			width : 'resolve',
			ajax : { // instead of writing the function to execute the request we use Select2's convenient helper
				url : "${contextPath}/crm/customer/query.json",
				dataType : 'json',
				params : {
					type : "POST"
				},
				allowClear : true,
				data : function(term, page) {
					return {
						"name" : term
					};
				},
				results : function(data, page) {
					var customers = data.customers;
					var results = [];
					$.each(customers, function(i, value) {
						var row = {
							id : value.id,
							text : value.name
						};
						results.push(row);
					});
					return {
						"results" : results
					};
				}
			}
		});

		$("#customerId").on("change", function(e) {
			var customerId = e.val;
			$.webtools.ajax({
				url : "${contextPath}/crm/custContacts/getContactsList.json",
				params : {
					"customerId" : customerId
				},
				isShowLoading : false,
				success : function(reply) {
					$("#contactId").empty();
					var custContactsList = reply.custContactsList;
					$.each(custContactsList, function(i, value) {
						$("#contactId").append("<option value='"+ value.id +"'>" + value.name + "</option>");
					});
				}
			});
		});
	</script>
</body>
</html>