<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/common.jspf"%>
<%@ include file="/common/taglibs.jspf"%>
<html lang="en">
<head>
<!-- bootstrap-datepicker -->
<link href="${contextPath}/static/widgets/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<script type="text/javascript" src="${contextPath}/static/widgets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<c:if test="${!empty SESSION_USER && !empty SESSION_USER.locale && !fn:startsWith(SESSION_USER.locale, 'en') }">
	<script src="${contextPath }/static/widgets/bootstrap-datepicker/js/locales/bootstrap-datepicker.${SESSION_USER.locale }.js"></script>
</c:if>
</head>
<body>
	<form id="theform" action="update.json" method="post" class="form-horizontal">
		<input type="hidden" name="id" value="${custActivity.id}" />
		<fieldset>
			<legend>
				<s:message code="custActivity.ui.fieldset.base" />
			</legend>
            <div class="row-fluid">
              <div class="control-group">
                <label class="control-label" for="customerId"><s:message code="custActivity.customerId" /></label>
                <div class="controls">
                  <input class="span5" type="hidden" id="customerId" name="customerId" value='${custActivity.customerId}' readonly="readonly" />
                </div>
              </div>
            </div>
            <div class="row-fluid">
              <div class="span6">
                <div class="control-group">
                  <label class="control-label" for="contactId"><s:message code="custActivity.contactId" /></label>
                  <div class="controls">
                    <select id="contactsId" name="contactsId" data-placeholder="<s:message code='common.ui.select' />">
                      <option value=""></option>
                      <li:optionsCollection collection="${custContactsList  }" var="row" value="${custActivity.contactId}">
                        <li:option property="${row.id }">${row.name }</li:option>
                      </li:optionsCollection>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-fluid">
              <div class="span6">
                <div class="control-group">
                  <label class="control-label" for="actType"><s:message code="custActivity.actType" /></label>
                  <div class="controls">
                    <select id="actType" name="actType" data-placeholder="<s:message code='common.ui.select' />">
                      <li:dictOptions dictType="custActivity-actType" dictValue="${custActivity.actType}"/>
                    </select>                    
                  </div>
                </div>
              </div>
            </div>
			<div class="row-fluid">
				<div class="span6">
					<div class="control-group">
						<label class="control-label" for="subject"><s:message code="custActivity.subject" /></label>
						<div class="controls">
							<input id="subject" name="subject" placeholder="" type="text" value='${custActivity.subject}' readonly="readonly" />
						</div>
					</div>
				</div>
			</div>

			<div class="row-fluid">
				<div class="span12">
					<div class="control-group">
						<label class="control-label" for="content"><s:message code="custActivity.content" /></label>
						<div class="controls">
							<textarea rows="5" cols="8" id="content" name="content" class="input-block-level limited">${custActivity.content}</textarea>
						</div>
					</div>
				</div>
			</div>

			<div class="row-fluid">
				<div class="span6">
					<div class="control-group">
						<label class="control-label" for="actDate"><s:message code="custActivity.actDate" /></label>
						<div class="controls">
							<div class="input-append date datepicker" data-date-format="yyyy-mm-dd">
								<input id="actDate" name="actDate"  type="text" value='${li:formatDate(custActivity.actDate)}' /><span class="add-on"><i class="icon-calendar"></i></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="control-group">
					<label class="control-label" for="nextActDate"><s:message code="custActivity.nextActDate" /></label>
					<div class="controls">
						<div class="input-append date datepicker" data-date-format="yyyy-mm-dd">
							<input id="nextActDate" name="nextActDate" type="text" value="${li:formatDate(custActivity.nextActDate)}" /> <span class="add-on"><i class="icon-calendar"></i></span>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row-fluid">
				<div class="span6">
					<div class="control-group">
						<label class="control-label" for="chargeUserId"><s:message code="custActivity.chargeBy" /></label>
						<div class="controls">
							<select id="chargeBy" name="chargeBy" data-placeholder="<s:message code='common.ui.select' />">
								<option value=""></option>
								<li:optionsCollection collection="${chargeUserList }" var="row" value="${custActivity.chargeBy}">
									<li:option property="${row.id }">${row.userName } (${row.loginId})</li:option>
								</li:optionsCollection>
							</select>

						</div>
					</div>
				</div>
			</div>

		</fieldset>

		<div class="form-actions">
			<button type="submit" class="btn btn-primary">
				<i class="icon-ok"></i>
				<s:message code="btn.save" />
			</button>
			<button type="button" class="btn" onclick="history.back();">
				<s:message code="btn.cancel" />
			</button>
		</div>

	</form>
	<!--page specific plugin scripts-->
	<script type="text/javascript">
		$('.datepicker').datepicker({
			todayBtn : true,
			todayHighlight : true
		});

		$('#theform').littFormSubmit({
			rules : {
				customerId : {
					required : true
				},
				subject : {
					required : true
				},
				chargeBy : {
					required : true
				}
			},
			success : function(reply) {
				location.href = <h:returnUrl value="index.do"></h:returnUrl>;
			}
		});

		$("#customerId").select2({
			width : 'resolve',
			ajax : { // instead of writing the function to execute the request we use Select2's convenient helper
				url : "${contextPath}/crm/customer/query.json",
				dataType : 'json',
				params : {
					type : "POST"
				},
				allowClear : true,
				data : function(term, page) {
					return {
						"name" : term
					};
				},
				results : function(data, page) {
					var customers = data.customers;
					var results = [];
					$.each(customers, function(i, value) {
						var row = {
							id : value.id,
							text : value.name
						};
						results.push(row);
					});
					return {
						"results" : results
					};
				}
			},
			initSelection : function(element, callback) {

				$.ajax("${contextPath}/crm/customer/getById.json", {
					data : {
						customerId : "${custActivity.customerId}"
					},
					dataType : 'json',
					params : {
						type : "POST"
					}
				}).done(function(data) {
					var customer = data.customer;
					var results = [];

					var row = {
						id : customer.id,
						text : customer.name
					};
					results.push(row);

					callback(row);
				});
			}
		});

		$("#customerId").on("change", function(e) {
			var customerId = e.val;
			$.webtools.ajax({
				url : "${contextPath}/crm/custContacts/getContactsList.json",
				params : {
					"customerId" : customerId
				},
				isShowLoading : false,
				success : function(reply) {
					$("#contactId").empty();
					var custContactsList = reply.custContactsList;
					$.each(custContactsList, function(i, value) {
						$("#contactId").append("<option value='"+ value.id +"'>" + value.name + "</option>");
					});
				}
			});
		});
	</script>
</body>
</html>