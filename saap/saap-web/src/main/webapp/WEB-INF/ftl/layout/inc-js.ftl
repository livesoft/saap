<!--[if lt IE 9]>
<script src="${extResUrl}/metronic/js/respond.min.js"></script>
<script src="${extResUrl}/metronic/js/excanvas.min.js"></script>
<![endif]-->
<script src="${extResUrl}/metronic/js/jquery.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/js/jquery-migrate.min.js" type="text/javascript"></script>

<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="${extResUrl}/metronic/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/js/jquery.blockui.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/js/jquery.cokie.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<#if plugin_validation!false>
<script src="${extResUrl}/metronic/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/plugins/jquery-validation/js/localization/messages_zh.min.js" type="text/javascript"></script>
<script>
    var validate_option = {
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        highlight: function (element) { // hightlight error inputs
            $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
        }
    }
</script>
</#if>
<script src="${extResUrl}/metronic/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/js/jquery.pulsate.min.js" type="text/javascript"></script>

<script src="${extResUrl}/metronic/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
<script src="${extResUrl}/metronic/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/js/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/js/bootstrap-dialog.min.js" type="text/javascript"></script>
<#if plugin_datatable!false>
<script src="${extResUrl}/metronic/plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>var msg_zh_cn={
    "sLoadingRecords":"读取中...",
    "sProcessing":"处理中.....",
    "sLengthMenu": "显示 _MENU_ 条记录/每页",
    "sZeroRecords": "没找到记录",
    "sInfo": "共_TOTAL_ 从 _START_ 到 _END_",
    "sInfoEmpty": "显示 0 条记录",
    "sInfoFiltered": "(从这些记录中 过滤出 _MAX_ 条记录)",
    'sSearch':'搜索:',
    "oPaginate" : {
        "sFirst":"首页",
        "sLast":"末页",
        "sPrevious": "前一页",
        "sNext": "下一页"
    }
};
function formatDate(date, dateformat) {
    if (date == "NaN")
        return null;
    var format = dateformat;
    var o = {
        "M+" : date.getMonth() + 1,
        "d+" : date.getDate(),
        "h+" : date.getHours(),
        "m+" : date.getMinutes(),
        "s+" : date.getSeconds(),
        "q+" : Math.floor((date.getMonth() + 3) / 3),
        "S" : date.getMilliseconds()
    };
    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (date.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
    }
    for ( var k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
                    : ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return format;
}
function dtRenderBool(data, type, row) {
    return (data || data.toString().toLowerCase()=='true') ? "是" : "否";
}
function dtRenderSex(data, type, row) {
    if ( data=='1') return '男';
    if (data =='2') return '女';
    return 'N/A';
}
function dtRenderDate(data, type, row){
    return data && data!="" ? formatDate(new Date(data),'yyyy-MM-dd h:s') : '';
}
</script>
</#if>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="${extResUrl}/metronic/js/metronic.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/layout/layout.js" type="text/javascript"></script>
<script src="${extResUrl}/metronic/layout/quick-sidebar.js" type="text/javascript"></script>
<#if plugin_tree!false>
<script src="${extResUrl}/metronic/plugins/ztree/js/jquery.ztree.core-3.5.min.js"></script>
<script src="${extResUrl}/metronic/plugins/ztree/js/jquery.ztree.excheck-3.5.min.js"></script>
</#if>

<#--<script src="${extResUrl}/metronic/layout/demo.js" type="text/javascript"></script>-->
<#attempt><#include '${mainView}-js.ftl' ignore_missing=true /><#recover></#attempt>
