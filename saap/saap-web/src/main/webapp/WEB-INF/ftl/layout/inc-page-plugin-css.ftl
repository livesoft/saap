<#if pageCss!?contains("tasks")><link href="${extResUrl}/metronic/css/opensans.css/opensans.css" rel="stylesheet" type="text/css"/></#if>
<#if plugin_datatable!false><link href="${extResUrl}/metronic/plugins/datatables/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/></#if>
<#if plugin_tree!false>
<link rel="stylesheet" type="text/css" href="${extResUrl}/metronic/plugins/ztree/css/zTreeStyle.css"/>
</#if>