<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                <form class="sidebar-search " action="extra_search.html" method="POST">
                    <a href="javascript:;" class="remove">
                        <i class="icon-close"></i>
                    </a>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span>
                    </div>
                </form>
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>
            <li class="start ${(!__moduleCode?has_content)?string('active','')} ">
                <a href="<@sp.url value="/" />">
                    <i class="icon-home"></i>
                    <span class="title">首页</span>
                </a>
                <li></li>
            </li>
            <@menu var="menuTree" user=SESSION_USER />
            <#--<@li.getMenuTree(SESSION_USER) />-->
            <#--<#assign menuTree=li.getMenuTree(SESSION_USER) />-->
            <#list menuTree as menu>
            <#if menu.isleaf>
            <li ${ (__moduleCode?starts_with(menu.menuCode))?string("class=\"active \"","")} >
                <a href="<@sp.url value="/menu.do?menuCode=${menu.menuCode}" />">
                    <i class="${menu.iconCls!}"></i>
                    <span class="title">${menu.menuName }</span>
                </a>
            </li>
            <#else>
            <li ${ (__moduleCode?starts_with(menu.menuCode))?string("class=\"active open\"","")} >
                <a href="###">
                    <i class="${menu.iconCls!}"></i>
                    <span class="title">${menu.menuName }</span>
                    <span class="arrow ${(__moduleCode?starts_with(menu.menuCode))?string("open","")}"></span>
                </a>

                <ul class="sub-menu">
                    <#list menu.subList as subMenu>
                    <li  ${ (subMenu.menuCode == __moduleCode )?string("class=\"active\"","")} >
                        <a href="<@sp.url value='/menu.do?menuCode=${subMenu.menuCode}' />">
                        <i class="${subMenu.iconCls!}"> </i>
                        ${subMenu.menuName }
                        </a>
                    </li>
                    </#list>
                </ul>
            </li>
            </#if>
            </#list>

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->
