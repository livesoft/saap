<#assign sp=JspTaglibs["http://www.springframework.org/tags"] >
<#assign form=JspTaglibs["http://www.springframework.org/tags/form"] >
<#assign li=JspTaglibs["http://www.littcore.com/core"] >
<#assign contextPath=request.contextPath />
<#if webConfig.extResUrl?has_content>
<#if webConfig.relatedToContext>
<#assign extResUrl="${contextPath}${webConfig.extResUrl}" />
<#else>
<#assign extResUrl="${webConfig.extResUrl}" />
</#if>
<#else>
<#assign extResUrl=request.contextPath />
</#if>

<!DOCTYPE html>
        <!-- ${contextPath} -->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>${projectTitle!}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <#attempt><#include "${mainView}-plugin.ftl" ignore_missing=true/><#recover ></#attempt>
    <#include 'layout/inc-global-css.ftl' />
    <!-- page plugin css-->
    <#include 'layout/inc-page-plugin-css.ftl' />
    <!-- page level css-->
    <#include 'layout/inc-page-level-css.ftl' />
    <#include 'layout/inc-theme-css.ftl' />
    <link rel="shortcut icon" href="favicon.ico"/>
    <script>var pageInit;</script>
</head>
<body  class="page-header-fixed page-quick-sidebar-over-content page-style-square">
<#include 'layout/inc-header.ftl' >
<div class="clearfix"></div>

<div class="page-container">
<#include 'layout/inc-menu.ftl' >

    <div class="page-content-wrapper">
        <div class="page-content">
        <#include "${mainPage}" >
        </div>
    </div>
</div>

<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        2015 &copy; <a href="http://www.littcore.com"> Littcore.com .</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->

<#include 'layout/inc-js.ftl' />
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        QuickSidebar.init(); // init quick sidebar
//        Demo.init(); // init demo features

        if (typeof(pageInit) != 'undefined') {
            pageInit();
        }
    });
</script>
</body>
</html>