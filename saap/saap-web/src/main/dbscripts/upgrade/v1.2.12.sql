DROP TABLE IF EXISTS TENANT_CONFIG;

/*==============================================================*/
/* Table: TENANT_CONFIG                                         */
/*==============================================================*/
CREATE TABLE TENANT_CONFIG
(
   ID                   INT NOT NULL AUTO_INCREMENT COMMENT '序号',
   TENANT_ID            INT NOT NULL COMMENT '租户ID',
   ATTR_KEY             VARCHAR(50) NOT NULL COMMENT '属性键',
   ATTR_VALUE           VARCHAR(500) COMMENT '属性值',
   PRIMARY KEY (ID)
)
ENGINE = INNODB;