DROP INDEX IDX_UC_MODULE ON USER_COMMENT;

DROP TABLE IF EXISTS USER_COMMENT;

/*==============================================================*/
/* Table: USER_COMMENT                                          */
/*==============================================================*/
CREATE TABLE USER_COMMENT
(
   ID                   INT NOT NULL AUTO_INCREMENT COMMENT '序号',
   TENANT_ID            INT NOT NULL COMMENT '租户ID',
   MODULE_CODE          VARCHAR(50) NOT NULL COMMENT '模块编号',
   RECORD_ID            INT NOT NULL COMMENT '数据ID',
   CONTENT              VARCHAR(2000) NOT NULL COMMENT '内容',
   CREATE_BY            INT NOT NULL COMMENT '创建人',
   CREATE_USERNAME      VARCHAR(50) NOT NULL COMMENT '显示用户名',
   CREATE_DATETIME      DATETIME NOT NULL COMMENT '创建时间',
   PRIMARY KEY (ID)
)
ENGINE = INNODB;

/*==============================================================*/
/* Index: IDX_UC_MODULE                                         */
/*==============================================================*/
CREATE INDEX IDX_UC_MODULE ON USER_COMMENT
(
   TENANT_ID,
   MODULE_CODE,
   RECORD_ID
);
