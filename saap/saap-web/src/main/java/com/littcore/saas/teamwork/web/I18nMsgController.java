package com.littcore.saas.teamwork.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Just for clear loaded message
 * Created by simon on 2014/12/29.
 */
@Controller
@RequestMapping("/msg")
public class I18nMsgController {

    @Autowired
    private MessageSource messageSource;

    @RequestMapping("/reload")
    public @ResponseBody String reload() {
        if (messageSource instanceof ReloadableResourceBundleMessageSource) {
            ((ReloadableResourceBundleMessageSource) messageSource).clearCache();
        }
        return "reloaded";
    }
}
