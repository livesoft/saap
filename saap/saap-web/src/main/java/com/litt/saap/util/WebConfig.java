package com.litt.saap.util;

import com.iteye.dwangel.util.BaseWebConfig;

/**
 * Pojo bean for configure properties by spring.
 * Created by simon on 2015/1/30.
 */
public class WebConfig extends BaseWebConfig {

    private boolean relatedToContext;

    public void init() {
        if (null == extResUrl) {
            // Try to use contextPath as url;
            extResUrl = "";
        }
    }

    public boolean isRelatedToContext() {
        return relatedToContext;
    }

    public void setRelatedToContext(boolean relatedToContext) {
        this.relatedToContext = relatedToContext;
    }
}
