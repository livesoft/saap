package com.litt.saap.system.po;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import java.io.Serializable;

/**
 * <br>
 * Table:app_store<br>
 * @author Hibernate Tools 3.4.0.CR1
 * @version 1.0
 * @since Feb 5, 2015 5:23:56 PM
 */
@Entity
@org.hibernate.annotations.Entity(dynamicUpdate = true, dynamicInsert = true)
@Table(name = "app_store", uniqueConstraints = @UniqueConstraint(columnNames = "AUTH_KEY"))
public class AppStore implements Serializable {
	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 组件标识（全局唯一）.
	 */
	private String uid;

	/**
	 * 授权KEY.
	 */
	private String authKey;

	/**
	 * 授权密钥.
	 */
	private String password;

	/**
	 * 名称.
	 */
	private String name;

	/**
	 * 类型
	        1：个人
	        2：企业
	        3：通用.
	 */
	private int type;

	/**
	 * 软件商编号.
	 */
	private String vendor;

	/**
	 * 授权方式.
	 */
	private int authType;

	/**
	 * 详细描述.
	 */
	private String description;

	/**
	 * 组件图标地址.
	 */
	private String iconUrl;

	/**
	 * 状态.
	 */
	private int status;

	/**
	 * 排行.
	 */
	private int position;

	/**
	 * 应用版本号.
	 */
	private String versionNo;

	/**
	 * 版本号.
	 */
	private int revision;

	/**
	 * 创建时间.
	 */
	private Date createDatetime;

	/**
	 * 更新时间.
	 */
	private Date updateDatetime;

	public AppStore() {
	}

	public AppStore(String uid, String authKey, String password, String name,
			int type, String vendor, int authType, String iconUrl, int status,
			int position, String versionNo, int revision, Date createDatetime,
			Date updateDatetime) {
		this.uid = uid;
		this.authKey = authKey;
		this.password = password;
		this.name = name;
		this.type = type;
		this.vendor = vendor;
		this.authType = authType;
		this.iconUrl = iconUrl;
		this.status = status;
		this.position = position;
		this.versionNo = versionNo;
		this.revision = revision;
		this.createDatetime = createDatetime;
		this.updateDatetime = updateDatetime;
	}

	public AppStore(String uid, String authKey, String password, String name,
			int type, String vendor, int authType, String description,
			String iconUrl, int status, int position, String versionNo,
			int revision, Date createDatetime, Date updateDatetime) {
		this.uid = uid;
		this.authKey = authKey;
		this.password = password;
		this.name = name;
		this.type = type;
		this.vendor = vendor;
		this.authType = authType;
		this.description = description;
		this.iconUrl = iconUrl;
		this.status = status;
		this.position = position;
		this.versionNo = versionNo;
		this.revision = revision;
		this.createDatetime = createDatetime;
		this.updateDatetime = updateDatetime;
	}

	/**  
	 * Get 组件标识（全局唯一）.
	 * @return 组件标识（全局唯一）
	 */
	@Id
	@Column(name = "UID", unique = true, nullable = false, length = 36)
	public String getUid() {
		return this.uid;
	}

	/**
	 * Set 组件标识（全局唯一）.
	 * @param uid 组件标识（全局唯一）
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}

	/**  
	 * Get 授权KEY.
	 * @return 授权KEY
	 */

	@Column(name = "AUTH_KEY", unique = true, nullable = false, length = 50)
	public String getAuthKey() {
		return this.authKey;
	}

	/**
	 * Set 授权KEY.
	 * @param authKey 授权KEY
	 */
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}

	/**  
	 * Get 授权密钥.
	 * @return 授权密钥
	 */

	@Column(name = "PASSWORD", nullable = false, length = 200)
	public String getPassword() {
		return this.password;
	}

	/**
	 * Set 授权密钥.
	 * @param password 授权密钥
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**  
	 * Get 名称.
	 * @return 名称
	 */

	@Column(name = "NAME", nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * Set 名称.
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**  
	 * Get 类型
	        1：个人
	        2：企业
	        3：通用.
	 * @return 类型
	        1：个人
	        2：企业
	        3：通用
	 */

	@Column(name = "TYPE", nullable = false)
	public int getType() {
		return this.type;
	}

	/**
	 * Set 类型
	        1：个人
	        2：企业
	        3：通用.
	 * @param type 类型
	        1：个人
	        2：企业
	        3：通用
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**  
	 * Get 软件商编号.
	 * @return 软件商编号
	 */

	@Column(name = "VENDOR", nullable = false, length = 50)
	public String getVendor() {
		return this.vendor;
	}

	/**
	 * Set 软件商编号.
	 * @param vendor 软件商编号
	 */
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	/**  
	 * Get 授权方式.
	 * @return 授权方式
	 */

	@Column(name = "AUTH_TYPE", nullable = false)
	public int getAuthType() {
		return this.authType;
	}

	/**
	 * Set 授权方式.
	 * @param authType 授权方式
	 */
	public void setAuthType(int authType) {
		this.authType = authType;
	}

	/**  
	 * Get 详细描述.
	 * @return 详细描述
	 */

	@Column(name = "DESCRIPTION", length = 65535)
	public String getDescription() {
		return this.description;
	}

	/**
	 * Set 详细描述.
	 * @param description 详细描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**  
	 * Get 组件图标地址.
	 * @return 组件图标地址
	 */

	@Column(name = "ICON_URL", nullable = false, length = 200)
	public String getIconUrl() {
		return this.iconUrl;
	}

	/**
	 * Set 组件图标地址.
	 * @param iconUrl 组件图标地址
	 */
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	/**  
	 * Get 状态.
	 * @return 状态
	 */

	@Column(name = "STATUS", nullable = false)
	public int getStatus() {
		return this.status;
	}

	/**
	 * Set 状态.
	 * @param status 状态
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**  
	 * Get 排行.
	 * @return 排行
	 */

	@Column(name = "POSITION", nullable = false)
	public int getPosition() {
		return this.position;
	}

	/**
	 * Set 排行.
	 * @param position 排行
	 */
	public void setPosition(int position) {
		this.position = position;
	}

	/**  
	 * Get 应用版本号.
	 * @return 应用版本号
	 */

	@Column(name = "VERSION_NO", nullable = false, length = 20)
	public String getVersionNo() {
		return this.versionNo;
	}

	/**
	 * Set 应用版本号.
	 * @param versionNo 应用版本号
	 */
	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

	/**  
	 * Get 版本号.
	 * @return 版本号
	 */

	@Column(name = "REVISION", nullable = false)
	public int getRevision() {
		return this.revision;
	}

	/**
	 * Set 版本号.
	 * @param revision 版本号
	 */
	public void setRevision(int revision) {
		this.revision = revision;
	}

	/**  
	 * Get 创建时间.
	 * @return 创建时间
	 */

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DATETIME", nullable = false, length = 19)
	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	/**
	 * Set 创建时间.
	 * @param createDatetime 创建时间
	 */
	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	/**  
	 * Get 更新时间.
	 * @return 更新时间
	 */

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATE_DATETIME", nullable = false, length = 19)
	public Date getUpdateDatetime() {
		return this.updateDatetime;
	}

	/**
	 * Set 更新时间.
	 * @param updateDatetime 更新时间
	 */
	public void setUpdateDatetime(Date updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

}
