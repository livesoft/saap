package com.litt.saap.core.module.tenant.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.litt.saap.core.module.tenant.config.meta.TenantMetaAttrGroup;


/**
 * .
 * 
 * <pre><b>描述：</b>
 *    
 * </pre>
 * 
 * <pre><b>修改记录：</b>
 *    
 * </pre>
 * 
 * @author <a href="mailto:littcai@hotmail.com">蔡源</a>
 * @since 2015年2月13日
 * @version 1.0
 */
public class TenantMetaConfig {
  
  private Map<String, TenantMetaAttrGroup> attrGroupMap = new HashMap<String, TenantMetaAttrGroup>();
  
  private TenantMetaAttrGroup[] attrGroupList;
  
  /**
   * Gets the attr group.
   *
   * @param code the code
   * @return the attr group
   */
  public TenantMetaAttrGroup getAttrGroup(String code)
  {
    return attrGroupMap.get(code);
  }
  
  /**
   * @return the attrGroupList
   */
  public TenantMetaAttrGroup[] getAttrGroupList()
  {
    return attrGroupList;
  }

  
  /**
   * @param attrGroupList the attrGroupList to set
   */
  public void setAttrGroupList(TenantMetaAttrGroup[] attrGroupList)
  {
    this.attrGroupList = attrGroupList;
    for (TenantMetaAttrGroup tenantMetaAttrGroup : attrGroupList)
    {
      attrGroupMap.put(tenantMetaAttrGroup.getCode(), tenantMetaAttrGroup);
    }
  }

  
  /**
   * @return the attrGroupMap
   */
  public Map<String, TenantMetaAttrGroup> getAttrGroupMap()
  {
    return attrGroupMap;
  }

}
