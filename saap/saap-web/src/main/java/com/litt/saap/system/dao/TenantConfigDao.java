package com.litt.saap.system.dao;

import java.util.List;

import com.litt.core.dao.GenericHibernateDao;
import com.litt.saap.system.po.TenantConfig;
import com.litt.saap.system.po.TenantMember;

public class TenantConfigDao extends GenericHibernateDao<TenantConfig, Integer> {
	
	public List<TenantConfig> listByTenant(int tenantId)
	  {
	    String hql = "from TenantConfig where tenantId=?";
	    List<TenantConfig> tenantConfigList = super.listAll(hql, new Object[]{tenantId});
	    return tenantConfigList;
	  }
	
	public void deleteByTenantId(int tenantId)
	  {
	    super.deleteBy("tenantId", tenantId);
	  }
	
}
