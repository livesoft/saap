package com.litt.saap.assistant.po;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.io.Serializable;

/**
 * <br>
 * Table:user_comment<br>
 * @author Hibernate Tools 3.4.0.CR1
 * @version 1.0
 * @since Apr 28, 2015 5:05:37 PM
 */
@Entity
@org.hibernate.annotations.Entity(dynamicUpdate = true, dynamicInsert = true)
@Table(name = "user_comment")
public class UserComment implements Serializable {
	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 序号.
	 */
	private Integer id;

	/**
	 * 租户ID.
	 */
	private int tenantId;

	/**
	 * 模块编号.
	 */
	private String moduleCode;

	/**
	 * 数据ID.
	 */
	private int recordId;

	/**
	 * 内容.
	 */
	private String content;

	/**
	 * 创建人.
	 */
	private int createBy;

	/**
	 * 显示用户名.
	 */
	private String createUsername;

	/**
	 * 创建时间.
	 */
	private Date createDatetime;

	public UserComment() {
	}

	public UserComment(int tenantId, String moduleCode, int recordId,
			String content, int createBy, String createUsername,
			Date createDatetime) {
		this.tenantId = tenantId;
		this.moduleCode = moduleCode;
		this.recordId = recordId;
		this.content = content;
		this.createBy = createBy;
		this.createUsername = createUsername;
		this.createDatetime = createDatetime;
	}

	/**  
	 * Get 序号.
	 * @return 序号
	 */
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	/**
	 * Set 序号.
	 * @param id 序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**  
	 * Get 租户ID.
	 * @return 租户ID
	 */

	@Column(name = "TENANT_ID", nullable = false)
	public int getTenantId() {
		return this.tenantId;
	}

	/**
	 * Set 租户ID.
	 * @param tenantId 租户ID
	 */
	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	/**  
	 * Get 模块编号.
	 * @return 模块编号
	 */

	@Column(name = "MODULE_CODE", nullable = false, length = 50)
	public String getModuleCode() {
		return this.moduleCode;
	}

	/**
	 * Set 模块编号.
	 * @param moduleCode 模块编号
	 */
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	/**  
	 * Get 数据ID.
	 * @return 数据ID
	 */

	@Column(name = "RECORD_ID", nullable = false)
	public int getRecordId() {
		return this.recordId;
	}

	/**
	 * Set 数据ID.
	 * @param recordId 数据ID
	 */
	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}

	/**  
	 * Get 内容.
	 * @return 内容
	 */

	@Column(name = "CONTENT", nullable = false, length = 2000)
	public String getContent() {
		return this.content;
	}

	/**
	 * Set 内容.
	 * @param content 内容
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**  
	 * Get 创建人.
	 * @return 创建人
	 */

	@Column(name = "CREATE_BY", nullable = false)
	public int getCreateBy() {
		return this.createBy;
	}

	/**
	 * Set 创建人.
	 * @param createBy 创建人
	 */
	public void setCreateBy(int createBy) {
		this.createBy = createBy;
	}

	/**  
	 * Get 显示用户名.
	 * @return 显示用户名
	 */

	@Column(name = "CREATE_USERNAME", nullable = false, length = 50)
	public String getCreateUsername() {
		return this.createUsername;
	}

	/**
	 * Set 显示用户名.
	 * @param createUsername 显示用户名
	 */
	public void setCreateUsername(String createUsername) {
		this.createUsername = createUsername;
	}

	/**  
	 * Get 创建时间.
	 * @return 创建时间
	 */

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DATETIME", nullable = false, length = 19)
	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	/**
	 * Set 创建时间.
	 * @param createDatetime 创建时间
	 */
	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

}
