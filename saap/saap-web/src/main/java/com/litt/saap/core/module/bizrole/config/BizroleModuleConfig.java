package com.litt.saap.core.module.bizrole.config;


/**
 * @author cwang
 */
public class BizroleModuleConfig {
	
	/** 业务编号 */
	private String moduleCode;	
	
	/** 租户 */
	private String tenantId;
	
	/**字段名**/
	private String fieldName;
	
	/**字段**/
	private String[] fields;
	
	public String getKey(){
		if(null != tenantId && !"".equals(tenantId))
			return tenantId + "&" + moduleCode;
		else
			return moduleCode;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String[] getFields() {
		return fields;
	}

	public void setFields(String[] fields) {
		this.fields = fields;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
}
