package com.litt.saap.system.web;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.litt.core.common.BeanManager;
import com.litt.core.module.annotation.Func;
import com.litt.core.web.mvc.action.BaseController;
import com.litt.saap.common.vo.LoginUserVo;
import com.litt.saap.core.common.SaapConstants;
import com.litt.saap.core.module.tenant.config.TenantMetaConfigManager;
import com.litt.saap.core.module.tenant.config.meta.TenantMetaAttr;
import com.litt.saap.core.module.tenant.config.meta.TenantMetaAttrGroup;
import com.litt.saap.core.web.util.LoginUtils;
import com.litt.saap.system.po.Tenant;
import com.litt.saap.system.po.TenantConfig;
import com.litt.saap.system.service.ITenantConfigService;
import com.litt.saap.system.service.ITenantService;

/**
 * TenantController.
 * 
 * <pre><b>描述：</b>
 *    
 * </pre>
 * 
 * <pre><b>修改记录：</b>
 *    
 * </pre>
 * 
 * @author <a href="mailto:littcai@hotmail.com">蔡源</a>
 * @since 2014年2月27日
 * @version 1.0
 */
@Controller
public class TenantController extends BaseController 
{

	@Resource
	private ITenantService tenantService;
	
	@Resource
	private ITenantConfigService tenantConfigService;	
	
	@Func(funcCode="query",moduleCode="tenant.tenant", enableLog=false)
	@RequestMapping
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response)
	{
		LoginUserVo loginUserVo = (LoginUserVo)LoginUtils.getLoginVo();
		
		Tenant tenant = tenantService.load(LoginUtils.getTenantId());
		//读取元数据
    TenantMetaAttrGroup[] metaAttrGroups = TenantMetaConfigManager.getInstance().getAllTenantMetaAttrGroup(); 
		
		return new ModelAndView("/system/tenant/index").addObject("tenant", tenant).addObject("metaAttrGroups", metaAttrGroups);
	}
	
	/**
	 * 配置空间.
	 * 
	 * @throws Exception
	 */
	@Func(funcCode="edit",moduleCode="tenant.tenant")
	@RequestMapping
	public ModelAndView config(@RequestParam String tenantAlias, HttpServletRequest request, HttpServletResponse response) throws Exception
	{				
		int tenantId = LoginUtils.getTenantId();
		
		tenantService.update(tenantId, tenantAlias);  //更新租户别名
		
		Map<String, String> paramMap = new HashMap<String, String>();
		//读取元数据
		TenantMetaAttrGroup[] metaAttrGroups = TenantMetaConfigManager.getInstance().getAllTenantMetaAttrGroup(); 
		for (TenantMetaAttrGroup attrGroup : metaAttrGroups)
    {
      String code = attrGroup.getCode();
      TenantMetaAttr[] attrs = attrGroup.getAttrList();
      for (TenantMetaAttr attr : attrs)
      {
        String fieldName = code + "_" + attr.getKey();
        String value = request.getParameter(fieldName);
        paramMap.put(code+"."+attr.getKey(), value);        
      }
    }
		
		tenantConfigService.updateBatch(tenantId, metaAttrGroups, paramMap);

	  //BeanManager.getServletContext().setAttribute(SaapConstants.APP_TENANT_CONFIG, );
	    
		
//		LoginUserVo loginUser = (LoginUserVo)LoginUtils.getLoginVo();
//		if(loginUser.getTenant()!=null)
//		{
//			loginUser.getTenant().setTenantAlias(tenantAlias);
//		}
		
		return new ModelAndView("jsonView");
	}
	
}
