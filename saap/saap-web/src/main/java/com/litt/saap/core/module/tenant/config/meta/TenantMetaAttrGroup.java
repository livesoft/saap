package com.litt.saap.core.module.tenant.config.meta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * .
 * 
 * <pre><b>描述：</b>
 *    
 * </pre>
 * 
 * <pre><b>修改记录：</b>
 *    
 * </pre>
 * 
 * @author <a href="mailto:littcai@hotmail.com">蔡源</a>
 * @since 2015年2月13日
 * @version 1.0
 */
public class TenantMetaAttrGroup {
  
  private String code;
  
  private Map<String, TenantMetaAttr> attrMap = new HashMap<String, TenantMetaAttr>();  //冗余数据，用于根据Attr的Key快速检索

  private TenantMetaAttr[] attrList;
  
  /**
   * @return the code
   */
  public String getCode()
  {
    return code;
  }

  
  /**
   * @param code the code to set
   */
  public void setCode(String code)
  {
    this.code = code;
  }

  
  /**
   * @return the attrMap
   */
  public Map<String, TenantMetaAttr> getAttrMap()
  {
    return attrMap;
  }
  
  /**
   * @return the attrList
   */
  public TenantMetaAttr[] getAttrList()
  {
    return attrList;
  }


  
  /**
   * @param attrList the attrList to set
   */
  public void setAttrList(TenantMetaAttr[] attrList)
  {
    this.attrList = attrList;
    for (TenantMetaAttr metaAttr : attrList)
    {
      attrMap.put(metaAttr.getKey(), metaAttr);
    }
  }

}
