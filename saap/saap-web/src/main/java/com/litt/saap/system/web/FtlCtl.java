package com.litt.saap.system.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * Created by simon on 2015/1/21.
 */
@Controller
public class FtlCtl {
    @RequestMapping("/test")
    public ModelAndView test() {
        ModelAndView mav = new ModelAndView();
//        mav.setViewName("test.ftl");
        return mav;
    }

}
