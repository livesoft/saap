package com.litt.saap.system.po;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.io.Serializable;

/**
 * <br>
 * Table:tenant_member<br>
 * @author Hibernate Tools 3.4.0.CR1
 * @version 1.0
 * @since Jan 29, 2015 1:38:08 PM
 */
@Entity
@org.hibernate.annotations.Entity(dynamicUpdate = true, dynamicInsert = true)
@Table(name = "tenant_member")
public class TenantMember implements Serializable {
	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 序号.
	 */
	private Integer id;

	/**
	 * 租赁ID.
	 */
	private int tenantId;

	/**
	 * 应用ID.
	 */
	private int appId;

	/**
	 * 用户ID.
	 */
	private Integer userId;

	/**
	 * 是否管理员.
	 */
	private boolean isAdmin;

	/**
	 * 状态.
	 */
	private int status;

	/**
	 * 创建人.
	 */
	private int createBy;

	/**
	 * 创建时间.
	 */
	private Date createDatetime;

	/**
	 * 创建人.
	 */
	private int updateBy;

	/**
	 * 更新时间.
	 */
	private Date updateDatetime;

	/**
	 * 成员类型.
	 */
	private int memberType;

	public TenantMember() {
	}

	public TenantMember(int tenantId, int appId, boolean isAdmin, int status,
			int createBy, Date createDatetime, int updateBy,
			Date updateDatetime, int memberType) {
		this.tenantId = tenantId;
		this.appId = appId;
		this.isAdmin = isAdmin;
		this.status = status;
		this.createBy = createBy;
		this.createDatetime = createDatetime;
		this.updateBy = updateBy;
		this.updateDatetime = updateDatetime;
		this.memberType = memberType;
	}

	public TenantMember(int tenantId, int appId, Integer userId,
			boolean isAdmin, int status, int createBy, Date createDatetime,
			int updateBy, Date updateDatetime, int memberType) {
		this.tenantId = tenantId;
		this.appId = appId;
		this.userId = userId;
		this.isAdmin = isAdmin;
		this.status = status;
		this.createBy = createBy;
		this.createDatetime = createDatetime;
		this.updateBy = updateBy;
		this.updateDatetime = updateDatetime;
		this.memberType = memberType;
	}

	/**  
	 * Get 序号.
	 * @return 序号
	 */
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	/**
	 * Set 序号.
	 * @param id 序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**  
	 * Get 租赁ID.
	 * @return 租赁ID
	 */

	@Column(name = "TENANT_ID", nullable = false)
	public int getTenantId() {
		return this.tenantId;
	}

	/**
	 * Set 租赁ID.
	 * @param tenantId 租赁ID
	 */
	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	/**  
	 * Get 应用ID.
	 * @return 应用ID
	 */

	@Column(name = "APP_ID", nullable = false)
	public int getAppId() {
		return this.appId;
	}

	/**
	 * Set 应用ID.
	 * @param appId 应用ID
	 */
	public void setAppId(int appId) {
		this.appId = appId;
	}

	/**  
	 * Get 用户ID.
	 * @return 用户ID
	 */

	@Column(name = "USER_ID")
	public Integer getUserId() {
		return this.userId;
	}

	/**
	 * Set 用户ID.
	 * @param userId 用户ID
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**  
	 * Get 是否管理员.
	 * @return 是否管理员
	 */

	@Column(name = "IS_ADMIN", nullable = false)
	public boolean getIsAdmin() {
		return this.isAdmin;
	}

	/**
	 * Set 是否管理员.
	 * @param isAdmin 是否管理员
	 */
	public void setIsAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	/**  
	 * Get 状态.
	 * @return 状态
	 */

	@Column(name = "STATUS", nullable = false)
	public int getStatus() {
		return this.status;
	}

	/**
	 * Set 状态.
	 * @param status 状态
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**  
	 * Get 创建人.
	 * @return 创建人
	 */

	@Column(name = "CREATE_BY", nullable = false)
	public int getCreateBy() {
		return this.createBy;
	}

	/**
	 * Set 创建人.
	 * @param createBy 创建人
	 */
	public void setCreateBy(int createBy) {
		this.createBy = createBy;
	}

	/**  
	 * Get 创建时间.
	 * @return 创建时间
	 */

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DATETIME", nullable = false, length = 19)
	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	/**
	 * Set 创建时间.
	 * @param createDatetime 创建时间
	 */
	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	/**  
	 * Get 创建人.
	 * @return 创建人
	 */

	@Column(name = "UPDATE_BY", nullable = false)
	public int getUpdateBy() {
		return this.updateBy;
	}

	/**
	 * Set 创建人.
	 * @param updateBy 创建人
	 */
	public void setUpdateBy(int updateBy) {
		this.updateBy = updateBy;
	}

	/**  
	 * Get 更新时间.
	 * @return 更新时间
	 */

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATE_DATETIME", nullable = false, length = 19)
	public Date getUpdateDatetime() {
		return this.updateDatetime;
	}

	/**
	 * Set 更新时间.
	 * @param updateDatetime 更新时间
	 */
	public void setUpdateDatetime(Date updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	/**  
	 * Get 成员类型.
	 * @return 成员类型
	 */

	@Column(name = "MEMBER_TYPE", nullable = false)
	public int getMemberType() {
		return this.memberType;
	}

	/**
	 * Set 成员类型.
	 * @param memberType 成员类型
	 */
	public void setMemberType(int memberType) {
		this.memberType = memberType;
	}

}
