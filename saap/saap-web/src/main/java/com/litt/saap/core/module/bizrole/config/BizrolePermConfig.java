package com.litt.saap.core.module.bizrole.config;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cwang
 */
public class BizrolePermConfig {
	
	/**
	 * key: moduleCode / tenantId&moduleCode
	 */
	private Map<String, BizroleModuleConfig> bizroleModeMap = new HashMap<String, BizroleModuleConfig>();
	
	private BizroleModuleConfig[] bizroleModuleList;

	public BizroleModuleConfig getBizroleModuleConfig(String code){
		return bizroleModeMap.get(code);
	}
	
	public BizroleModuleConfig[] getBizroleModuleList() {
		return bizroleModuleList;
	}
	public void setBizroleModuleList(BizroleModuleConfig[] bizroleModuleList) {
		this.bizroleModuleList = bizroleModuleList;
		for(BizroleModuleConfig bizroleModule : bizroleModuleList){
			bizroleModeMap.put(bizroleModule.getKey(), bizroleModule);
		}
	}
}
