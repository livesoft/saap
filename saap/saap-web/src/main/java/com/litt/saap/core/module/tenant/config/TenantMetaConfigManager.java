package com.litt.saap.core.module.tenant.config;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.litt.saap.core.module.tenant.config.meta.TenantMetaAttrGroup;
import com.litt.saap.core.util.ConfigUtils;
import com.litt.saap.system.vo.TenantConfVo;


/**
 * .
 * 
 * <pre><b>描述：</b>
 *    
 * </pre>
 * 
 * <pre><b>修改记录：</b>
 *    
 * </pre>
 * 
 * @author <a href="mailto:littcai@hotmail.com">蔡源</a>
 * @since 2015年2月13日
 * @version 1.0
 */
public class TenantMetaConfigManager {
  
  public static final String CONF_MAPPING_FILE_PATH = "/module/tenant-meta-mapping.xml";
  
  public static final String CONF_FILE_PATH = "classpath:module/tenant-meta.xml";
  
  private TenantMetaConfig config;
  
  /** 租户配置全局缓存. */
  private Map<Integer, TenantConfVo> tenantConfMapCache = new ConcurrentHashMap<Integer, TenantConfVo>();

  public TenantMetaConfigManager()
  {
    init();
  }
  
  public void init()
  {
    //读取配置文件
    this.config = ConfigUtils.loadByCastor(TenantMetaConfig.class, CONF_MAPPING_FILE_PATH, CONF_FILE_PATH);       
  } 
  
  public void reload() {
    
    this.config = ConfigUtils.loadByCastor(TenantMetaConfig.class, CONF_MAPPING_FILE_PATH, CONF_FILE_PATH);
  } 
  
  public TenantMetaAttrGroup getTenantMetaAttrGroup(String code)
  {
    return config.getAttrGroup(code);
  }
  
  public TenantMetaAttrGroup[] getAllTenantMetaAttrGroup()
  {
    return config.getAttrGroupList();
  }
  
  
  public static void main(String[] args) throws Exception {
    TenantMetaConfig config = TenantMetaConfigManager.getInstance().getConfig();
    System.out.println(config.getAttrGroup("companyInfo"));
    System.out.println(config.getAttrGroupList().length);
  }
  
  private static class SingletonClassInstance { 
      private static final TenantMetaConfigManager instance = new TenantMetaConfigManager(); 
  } 

  /**
   * 获得实例对象.
   * @return
   */
  public static TenantMetaConfigManager getInstance() { 
      return SingletonClassInstance.instance; 
  }
  
  /**
   * @return the config
   */
  public TenantMetaConfig getConfig() {
    return config;
  }

  
  public Map<Integer, TenantConfVo> getTenantConfCache()
  {
    return tenantConfMapCache;
  }

}
