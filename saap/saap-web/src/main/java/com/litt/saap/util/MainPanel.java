package com.litt.saap.util;

import javax.swing.*;
import java.awt.*;

/**
 * Use this panel to collect input values.
 * Created by simon on 2014/12/25.
 */
public class MainPanel extends JPanel {

    private JTextField fieldDbHost;
    private JTextField fieldUser;
    private JTextField fieldPassword;
    private JTextField fieldDbName;

    public void initGui(){
        fieldDbHost = new JTextField("localhost");
        fieldUser = new JTextField("root");
        fieldPassword = new JTextField("000000");
        fieldDbName = new JTextField("saap");
        GridLayout layout = new GridLayout(4,2);
        this.setLayout(layout);
        this.add(new JLabel("数据库服务器地址"));
        this.add(fieldDbHost);
        this.add(new JLabel("数据库用户名"));
        this.add(fieldUser);
        this.add(new JLabel("数据库密码"));
        this.add(fieldPassword);
        this.add(new JLabel("目标数据库名"));
        this.add(fieldDbName);

    }

    public String getDbHost() {
        return fieldDbHost.getText();
    }
    public String getUser() {
        return fieldUser.getText();
    }
    public String getPassword() {
        return fieldPassword.getText();
    }
    public String getDbName() {
        return fieldDbName.getText();
    }

    /**
     * Method to show the form.
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        MainPanel panel = new MainPanel();
        panel.initGui();
        frame.setContentPane(panel);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public void setEnable(boolean flag) {
        fieldDbHost.setEnabled(flag);
        fieldUser.setEnabled(flag);
        fieldPassword.setEnabled(flag);
        fieldDbName.setEnabled(flag);
    }
}
