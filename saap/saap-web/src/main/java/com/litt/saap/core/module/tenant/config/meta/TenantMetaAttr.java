package com.litt.saap.core.module.tenant.config.meta;

import java.io.Serializable;


/**
 * .
 * 
 * <pre><b>描述：</b>
 *    
 * </pre>
 * 
 * <pre><b>修改记录：</b>
 *    
 * </pre>
 * 
 * @author <a href="mailto:littcai@hotmail.com">蔡源</a>
 * @since 2015年2月13日
 * @version 1.0
 */
public class TenantMetaAttr implements  Serializable {
  
  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  private String key;
  
  private String dataType = "string";
  
  private String displayName;
  
  private String descr;
  
  private String dataUnit;
  
  /**
   * 该节点属性是否可修改，默认为true,即可修改。
   */
  private boolean editable = true;
  
  private boolean required = true;
  
  private String defaultValue;

  
  /**
   * @return the key
   */
  public String getKey()
  {
    return key;
  }

  
  /**
   * @param key the key to set
   */
  public void setKey(String key)
  {
    this.key = key;
  }

  
  /**
   * @return the dataType
   */
  public String getDataType()
  {
    return dataType;
  }

  
  /**
   * @param dataType the dataType to set
   */
  public void setDataType(String dataType)
  {
    this.dataType = dataType;
  }

  
  /**
   * @return the displayName
   */
  public String getDisplayName()
  {
    return displayName;
  }

  
  /**
   * @param displayName the displayName to set
   */
  public void setDisplayName(String displayName)
  {
    this.displayName = displayName;
  }

  
  /**
   * @return the descr
   */
  public String getDescr()
  {
    return descr;
  }

  
  /**
   * @param descr the descr to set
   */
  public void setDescr(String descr)
  {
    this.descr = descr;
  }

  
  /**
   * @return the dataUnit
   */
  public String getDataUnit()
  {
    return dataUnit;
  }

  
  /**
   * @param dataUnit the dataUnit to set
   */
  public void setDataUnit(String dataUnit)
  {
    this.dataUnit = dataUnit;
  }

  
  /**
   * @return the editable
   */
  public boolean isEditable()
  {
    return editable;
  }

  
  /**
   * @param editable the editable to set
   */
  public void setEditable(boolean editable)
  {
    this.editable = editable;
  }

  
  /**
   * @return the required
   */
  public boolean isRequired()
  {
    return required;
  }

  
  /**
   * @param required the required to set
   */
  public void setRequired(boolean required)
  {
    this.required = required;
  }

  
  /**
   * @return the defaultValue
   */
  public String getDefaultValue()
  {
    return defaultValue;
  }

  
  /**
   * @param defaultValue the defaultValue to set
   */
  public void setDefaultValue(String defaultValue)
  {
    this.defaultValue = defaultValue;
  }

}
