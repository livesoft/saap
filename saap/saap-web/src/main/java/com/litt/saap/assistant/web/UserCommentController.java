package com.litt.saap.assistant.web;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.litt.core.common.Utility;
import com.litt.core.dao.page.IPageList;
import com.litt.core.dao.ql.PageParam;
import com.litt.core.exception.NotLoginException;
import com.litt.core.web.mvc.action.BaseController;
import com.litt.core.web.util.WebUtils;
import com.litt.saap.assistant.po.UserComment;
import com.litt.saap.assistant.service.IUserCommentService;
import com.litt.saap.assistant.vo.UserCommentVo;
import com.litt.saap.common.vo.LoginUserVo;


/**
 * .
 * 
 * <pre><b>描述：</b>
 *    
 * </pre>
 * 
 * <pre><b>修改记录：</b>
 *    
 * </pre>
 * 
 * @author <a href="mailto:littcai@hotmail.com">蔡源</a>
 * @since 2015年4月28日
 * @version 1.0
 */
@Controller
public class UserCommentController extends BaseController {

  @Resource
  private IUserCommentService userCommentService;
  
  @RequestMapping 
  public ModelAndView save(WebRequest request) throws Exception
  { 
    UserComment userComment = new UserComment();
    BeanUtils.populate(userComment, request.getParameterMap());    
    LoginUserVo loginVo = (LoginUserVo)super.getLoginVo();
    
    userComment.setTenantId(loginVo.getTenantId());
    userComment.setCreateBy(loginVo.getOpId().intValue());
    userComment.setCreateUsername(loginVo.getOpName());
    UserCommentVo vo = userCommentService.save(userComment);
    
    return new ModelAndView("jsonView").addObject("userComment", vo); 
  }
  
  /**
   * Delete.
   * @param id id
   * @throws Exception 
   */
  @RequestMapping 
  public void delete(@RequestParam Integer id) throws Exception
  {
    userCommentService.delete(id);
  }
  
  @RequestMapping 
  public ModelAndView getComments(WebRequest request) throws NotLoginException
  { 
    LoginUserVo loginUserVo = (LoginUserVo)super.getLoginVo();
            
    String moduleCode = request.getParameter("moduleCode");
    Integer dataId = Utility.parseInt(request.getParameter("dataId"), null);
    
    //package the params
    PageParam pageParam = WebUtils.getPageParam(request);
    pageParam.addCond("tenantId", loginUserVo.getTenantId());
    pageParam.addCond("moduleCode", moduleCode);  
    pageParam.addCond("dataId", dataId);  
    pageParam.addCond("isDeleted", false);  
    pageParam.addSort("createDatetime", "desc");
    //Get page result
    IPageList pageList = userCommentService.findPage(pageParam);   
    
    return new ModelAndView("jsonView").addObject("userComments", pageList.getRsList()); 
  
  }   
}
