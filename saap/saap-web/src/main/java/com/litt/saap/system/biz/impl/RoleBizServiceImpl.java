package com.litt.saap.system.biz.impl;

import java.util.List;

import javax.annotation.Resource;

import com.litt.core.util.BeanCopier;
import com.litt.saap.system.biz.IRoleBizService;
import com.litt.saap.system.dao.RoleDao;
import com.litt.saap.system.po.Role;
import com.litt.saap.system.vo.RoleVo;
import com.litt.saap.system.webservice.IRoleWebService;

public class RoleBizServiceImpl implements IRoleWebService, IRoleBizService {
	
	@Resource
	private RoleDao roleDao;
	
	@Override
	public List<RoleVo> listByUser(int userId) {
		String listHql = "select r from Role r, UserRole ur where r.id=ur.roleId and ur.userId=?";
		List<Role> roles = roleDao.listAll(listHql, new Object[]{userId});
		return BeanCopier.copyList(roles, RoleVo.class);
	}
	
	
	
}
