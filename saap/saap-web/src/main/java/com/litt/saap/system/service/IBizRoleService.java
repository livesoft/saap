package com.litt.saap.system.service;

import java.util.List;
import java.util.Map;

import com.litt.core.dao.page.IPageList;
import com.litt.core.dao.ql.PageParam;
import com.litt.saap.system.po.BizRole;
import com.litt.saap.system.po.BizRoleDataPermission;
import com.litt.saap.system.po.BizRoleMember;

/**
 * .
 * 
 * <pre><b>描述：</b>
 *    
 * </pre>
 * 
 * <pre><b>修改记录：</b>
 *    
 * </pre>
 * 
 * @author <a href="mailto:littcai@hotmail.com">蔡源</a>
 * @since 2013-11-14
 * @version 1.0
 */
public interface IBizRoleService {

	/**
	 * Save.
	 *
	 * @param role the role
	 * @param permissionCodes the permission codes
	 * @return the role
	 */
	public BizRole save(BizRole role, String[] moduleCodes, String[] fields);
	
	/**
	 * Update.
	 *
	 * @param role Role
	 * @param permissionCodes the permission codes
	 */
	public void update(BizRole role, String[] moduleCodes, String[] fields);	
	
	/**
	 * Delete by id.
	 * @param id id
	 */
	public void delete(Integer id);	
	
	/**
	 * Delete by instance.
	 * @param id id
	 */
	public void delete(BizRole role);
	
	/**
	 * Batch delete by ids.
	 * @param ids ids
	 */
	public void deleteBatch(Integer[] ids);
	
	/**
	 * Load.
	 *
	 * @param roleId the role id
	 * @return the role
	 */
	public BizRole load(int roleId);
	
	/**
	 * load member by tenant.
	 *
	 * @param tenantId the tenant id
	 * @return the list
	 */
	public BizRole load(int tenantId, String name, int status);
	
	/**
	 * List by tenant.
	 *
	 * @param tenantId the tenant id
	 * @return the list
	 */
	public List<BizRole> listByTenant(int tenantId);
	
	public List<BizRole> listByTenantAndUser(int tenantId, int userId);
	
	/**
	 * list by page.
	 * 
	 * @param pageParam params
	 * @return IPageList IPageList
	 */
	public IPageList listPage(PageParam pageParam);	
	
	/**
	 * 业务角色权限树
	 * @return
	 */
	public List<Map> findBizRolePermissionTree(int tenantId, Integer bizRoleId);
	
	/**
	 * 业务角色成员
	 * @param tenantId
	 * @param bizRoleId
	 * @return
	 */
	public List<BizRoleMember> listBizRoleMember(int tenantId, Integer bizRoleId);
	
	/**
	 * 业务角色数据权限
	 * @param tenantId
	 * @param bizRoleId
	 * @return
	 */
	public List<BizRoleDataPermission> listBizRoleDataPermission(int tenantId, Integer bizRoleId);
	
	public List<BizRoleDataPermission> listBizRoleDataPermission(int tenantId, Integer bizRoleId, String fieldName);
	
	public String[] findUserDataPermissions(int tenantId, int userId, String fieldName);
	
	/**
	 * Update.
	 *
	 * @param role Role
	 * @param permissionCodes the permission codes
	 */
	public void updateBizRoleMember(BizRole role, String[] roleMemberIds);	
	
	/**
	 * Update.
	 *
	 * @param role Role
	 * @param permissionCodes the permission codes
	 */
	public void updateBizRoleDataPermission(BizRole role, String data, String fieldName);	
	
	/**
	 * Resume by id.
	 * @param id id
	 */
	public void doResume(Integer id) ;

}