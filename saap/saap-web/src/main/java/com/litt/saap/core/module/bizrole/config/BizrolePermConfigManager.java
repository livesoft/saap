package com.litt.saap.core.module.bizrole.config;

import java.util.Arrays;

import com.litt.saap.core.util.ConfigUtils;

/**
 * @author cwang
 */
public class BizrolePermConfigManager {
	
	public static final String CONF_MAPPING_FILE_PATH = "/module/bizrole/bizrole-perm-conf-mapping.xml";
	
	public static final String CONF_FILE_PATH = "classpath:module/bizrole/bizrole-perm-conf.xml";
	
	private BizrolePermConfig config;
	
	public BizrolePermConfigManager()
	{
		init();
	}
	
	public void init()
	{
		//读取配置文件
		this.config = ConfigUtils.loadByCastor(BizrolePermConfig.class, CONF_MAPPING_FILE_PATH, CONF_FILE_PATH);				
	}	
	
	public void reload() {
		
		this.config = ConfigUtils.loadByCastor(BizrolePermConfig.class, CONF_MAPPING_FILE_PATH, CONF_FILE_PATH);
	} 
	
	public BizroleModuleConfig getBizroleModuleConfig(String code)
	{
		return config.getBizroleModuleConfig(code);
	}
	
	
	public static void main(String[] args) throws Exception {
		BizrolePermConfig config = BizrolePermConfigManager.getInstance().getConfig();
		System.out.println(config.getBizroleModuleConfig("test1&test1"));
		System.out.println(config.getBizroleModuleConfig("test2"));
	}
	
	private static class SingletonClassInstance { 
	    private static final BizrolePermConfigManager instance = new BizrolePermConfigManager(); 
	} 

	/**
	 * 获得实例对象.
	 * @return
	 */
	public static BizrolePermConfigManager getInstance() { 
	    return SingletonClassInstance.instance; 
	}
	
	/**
	 * @return the config
	 */
	public BizrolePermConfig getConfig() {
		return config;
	}
	
}
