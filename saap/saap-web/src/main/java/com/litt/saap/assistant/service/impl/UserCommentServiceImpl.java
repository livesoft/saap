package com.litt.saap.assistant.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.litt.core.dao.IResultsetTransformer;
import com.litt.core.dao.page.IPageList;
import com.litt.core.dao.ql.PageParam;
import com.litt.core.util.BeanCopier;
import com.litt.saap.assistant.dao.UserCommentDao;
import com.litt.saap.assistant.po.UserComment;
import com.litt.saap.assistant.service.IUserCommentService;
import com.litt.saap.assistant.vo.UserCommentVo;
import com.litt.saap.assistant.webservice.IUserCommentWebService;
import com.litt.saap.common.vo.IUserInfo;
import com.litt.saap.system.service.IUserInfoService;

/**
 * 
 * Comment service impl.
 * <pre><b>Description：</b>
 *    User Coomment
 * </pre>
 * 
 * @author <a href="mailto:littcai@hotmail.com">Bob.cai</a>
 * @since 2013-11-19
 * @version 1.0
 */
public class UserCommentServiceImpl implements IUserCommentService, IUserCommentWebService
{ 
	/** Logger. */
  private static final Logger logger = LoggerFactory.getLogger(UserCommentServiceImpl.class);
    
  @Resource
  private UserCommentDao userCommentDao;
  @Resource
  private IUserInfoService userInfoService;
    
  /* (non-Javadoc)
   * @see com.litt.saap.assistant.service.impl.IUserCommentWebService#save(int, int, java.lang.String, int, java.lang.String)
   */
  @Override
  public UserCommentVo save(int tenantId, int userId, String moduleCode, int dataId, String content)
  {
    UserComment userComment = new UserComment();
    userComment.setTenantId(tenantId);
    userComment.setModuleCode(moduleCode);
    userComment.setRecordId(dataId);
    userComment.setContent(content);
    userComment.setCreateBy(userId);
    IUserInfo userInfo = userInfoService.find(userId);
    userComment.setCreateUsername(userInfo.getUserName()); 
    userComment.setCreateDatetime(new Date());
    return this.save(userComment);
  }

   	/**
	 * Save.
	 * @param userComment UserComment
	 * @return id
	 */
	public UserCommentVo save(UserComment userComment)
	{
	  userComment.setCreateDatetime(new Date());
		userCommentDao.save(userComment);
		UserCommentVo userCommentVo = BeanCopier.copy(userComment, UserCommentVo.class);
		IUserInfo userInfo = userInfoService.find(userCommentVo.getCreateBy());
    userCommentVo.setCreateUserAvatar(userInfo.getHeadImgUrl());
		return userCommentVo;
	}
	
   	/**
	 * Update.
	 * @param userComment UserComment
	 */
	public void update(UserComment userComment)
	{
		userCommentDao.update(userComment);
	}			
   
   	/* (non-Javadoc)
     * @see com.litt.saap.assistant.service.impl.IUserCommentWebService#delete(java.lang.Integer)
     */
	@Override
  public void delete(Integer id)
	{
		userCommentDao.delete(UserComment.class, "id", id);
	}
	
	/**
	 * Delete by instance.
	 * @param id id
	 */
	public void delete(UserComment userComment)
	{
		userCommentDao.delete(userComment);
	}
	
	/**
	 * Load by id.
	 * @param id id
	 * @return UserComment
	 */
	public UserComment load(Integer id)
	{
		return userCommentDao.load(UserComment.class, id);
	}
	
	/**
	 * list by page.
	 * 
	 * @param pageParam params
	 * @return IPageList IPageList
	 */
	public IPageList listPage(PageParam pageParam)
	{
		String listHql = "select obj from UserComment obj"
			+ "-- and obj.tenantId={tenantId}"
			+ "-- and obj.moduleCode={moduleCode}"
			+ "-- and obj.dataId={dataId}"
			;	
		return userCommentDao.listPage(listHql, pageParam);
	}
	
	/* (non-Javadoc)
   * @see com.litt.saap.assistant.service.impl.IUserCommentWebService#findPage(com.litt.core.dao.ql.PageParam)
   */
	@Override
  public IPageList findPage(PageParam pageParam)
	{
	  IPageList pageList = this.listPage(pageParam);
	  pageList.setResultsetTransformer(new IResultsetTransformer(){

	    @Override
      public List transform(List srcList)
      {
        List<UserCommentVo> voList = BeanCopier.copyList(srcList, UserCommentVo.class);
        for (UserCommentVo userCommentVo : voList)
        {
          IUserInfo userInfo = userInfoService.find(userCommentVo.getCreateBy());
          userCommentVo.setCreateUserAvatar(userInfo.getHeadImgUrl());
        }
        return voList;
      }
	    
	  });
	  return pageList;
	}
}