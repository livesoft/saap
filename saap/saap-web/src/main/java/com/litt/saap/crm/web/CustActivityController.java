
package com.litt.saap.crm.web;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.litt.core.common.Utility;
import com.litt.core.dao.page.IPageList;
import com.litt.core.dao.ql.PageParam;
import com.litt.core.exception.NotLoginException;
import com.litt.core.module.annotation.Func;
import com.litt.core.web.mvc.action.BaseController;
import com.litt.core.web.util.WebUtils;
import com.litt.saap.common.vo.LoginUserVo;
import com.litt.saap.common.vo.TenantUserVo;
import com.litt.saap.core.web.util.LoginUtils;
import com.litt.saap.crm.po.CustActivity;
import com.litt.saap.crm.service.ICustActivityService;
import com.litt.saap.crm.vo.CustContactsVo;
import com.litt.saap.crm.webservice.ICustContactsWebService;
import com.litt.saap.system.webservice.IUserWebService;

/**
 * 
 * Activity controller.
 * <pre><b>Description：</b>
 *    Customer Activities Management
 * </pre>
 * 
 * @author <a href="mailto:littcai@hotmail.com">Bob.cai</a>
 * @since 2014-05-19
 * @version 1.0
 */
@Controller
public class CustActivityController extends BaseController
{
	private final static Logger logger = LoggerFactory.getLogger(CustActivityController.class);

	@Resource
	private ICustActivityService custActivityService;
	
	@Resource
	private IUserWebService userWebService;
	
	@Resource
	private ICustContactsWebService custContactsWebService;
	
	/**
	 * default page.
	 * 
	 * @param request the request
	 * @param respnose the respnose
	 * 
	 * @return ModelAndView
	 */	
	@Func(funcCode="query", moduleCode="crm.custActivity", enableLog=false) 
	@RequestMapping 
	public ModelAndView index(WebRequest request, ModelMap modelMap) throws NotLoginException
	{	
		LoginUserVo loginUserVo = (LoginUserVo)super.getLoginVo();
		
		//get params from request
		int customerId = Utility.parseInt(request.getParameter("customerId"));
		int contactId = Utility.parseInt(request.getParameter("contactId"));
				
		//package the params
		PageParam pageParam = WebUtils.getPageParam(request);
		pageParam.addCond("tenantId", loginUserVo.getTenantId());
		pageParam.addCond("customerId", customerId==0?null:customerId);	
		pageParam.addCond("contactId", contactId==0?null:contactId);	
		pageParam.addCond("isDeleted", false);	
		//Get page result
		IPageList pageList = custActivityService.listPage(pageParam);		
		
		//return params to response
		modelMap.addAttribute("pageParam", pageParam);	
		modelMap.addAttribute("pageList", pageList);	
		
    return new ModelAndView("/crm/custActivity/index");	
	
	} 	
	
	/**
	 * Add Page.
	 * 
	 * @return ModelAndView
	 */	
	@Func(funcCode="add", moduleCode="crm.custActivity", enableLog=false)  
	@RequestMapping
	public ModelAndView add() 
	{ 
	  Date now = new Date();
	  int currentUserId = super.getLoginOpId().intValue();
		List<TenantUserVo> chargeUserList = userWebService.findByTenant(LoginUtils.getTenantId());
  	return new ModelAndView("/crm/custActivity/add")
  	    .addObject("now", now)
  	    .addObject("currentUserId", currentUserId)
  	    .addObject("chargeUserList", chargeUserList);
  }
	
	/**
	 * Edit Page.
	 * 
	 * @param id 
	 * 
	 * @return ModelAndView
	 */
	@Func(funcCode="edit", moduleCode="crm.custActivity", enableLog=false)  
	@RequestMapping 
	public ModelAndView edit(@RequestParam Integer id) 
	
	{ 
		List<TenantUserVo> chargeUserList = userWebService.findByTenant(LoginUtils.getTenantId());
		CustActivity custActivity = custActivityService.load(id);
		List<CustContactsVo> custContactsList = custContactsWebService.findByCustomer(custActivity.getCustomerId());
		
        return new ModelAndView("/crm/custActivity/edit")
        .addObject("custActivity", custActivity)
        .addObject("chargeUserList", chargeUserList)
        .addObject("custContactsList",custContactsList);
    }	
    
	/**
	 * Show Page.
	 * 
	 * @param id 
	 * 
	 * @return ModelAndView
	 */
	@Func(funcCode="query", moduleCode="crm.custActivity", enableLog=false)  
	@RequestMapping 
	public ModelAndView show(@RequestParam Integer id) 
	{ 
		CustActivity custActivity = custActivityService.load(id);		
        return new ModelAndView("/crm/custActivity/show").addObject("custActivity", custActivity);
    }   
    
    /**
	 * Save.
	 * @param request 
	 * @param modelMap
	 * @throws Exception 
	 */
	@Func(funcCode="add",moduleCode="crm.custActivity")
	@RequestMapping 
	public void save(WebRequest request, ModelMap modelMap) throws Exception
	{	
		int tenantId = LoginUtils.getTenantId();
		int userId = LoginUtils.getLoginOpId().intValue();
		
		CustActivity custActivity = new CustActivity();
		BeanUtils.populate(custActivity, request.getParameterMap());
		
		custActivity.setTenantId(tenantId);
		custActivity.setCreateBy(userId);
		
		custActivityService.save(custActivity);
	}
	
	/**
	 * Update.
	 * @param request 
	 * @param modelMap
	 * @throws Exception 
	 */
	@Func(funcCode="edit",moduleCode="crm.custActivity")
	@RequestMapping 
	public void update(WebRequest request, ModelMap modelMap) throws Exception
	{
		int userId = LoginUtils.getLoginOpId().intValue();
		
		CustActivity custActivity = custActivityService.load(Utility.parseInt(request.getParameter("id")));
		BeanUtils.populate(custActivity, request.getParameterMap());
		custActivityService.update(custActivity);
	}
	
	/**
	 * Delete.
	 * @param id id
	 * @throws Exception 
	 */
	@Func(funcCode="delete",moduleCode="crm.custActivity")
	@RequestMapping 
	public void delete(@RequestParam Integer id) throws Exception
	{
		custActivityService.delete(id);
	}

	/**
	 * Delete Batch.
	 * @param id id
	 * @throws Exception 
	 */
	@Func(funcCode="delete",moduleCode="crm.custActivity")
	@RequestMapping 
	public void deleteBatch(@RequestParam(value="ids[]") Integer[] ids) throws Exception
	{
		custActivityService.deleteBatch(ids);
	}

}
