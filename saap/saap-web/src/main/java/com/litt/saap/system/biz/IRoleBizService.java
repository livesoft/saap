package com.litt.saap.system.biz;

import java.util.List;

import com.litt.saap.system.vo.RoleVo;


public interface IRoleBizService {
	
	public List<RoleVo> listByUser(int userId);

}