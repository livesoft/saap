package com.litt.saap.system.po;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.io.Serializable;

/**
 * <br>
 * Table:user_ext<br>
 * @author Hibernate Tools 3.4.0.CR1
 * @version 1.0
 * @since Apr 28, 2015 1:23:09 PM
 */
@Entity
@org.hibernate.annotations.Entity(dynamicUpdate = true, dynamicInsert = true)
@Table(name = "user_ext")
public class UserExt implements Serializable {
	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 用户ID.
	 */
	private int id;

	/**
	 * 用户等级.
	 */
	private Integer level;

	/**
	 * 用户来源.
	 */
	private String source;

	/**
	 * 生日.
	 */
	private Date birthday;

	/**
	 * 邮政编码.
	 */
	private String zipCode;

	/**
	 * 联系地址.
	 */
	private String address;

	/**
	 * 区.
	 */
	private String district;

	/**
	 * 城市.
	 */
	private String city;

	/**
	 * 省份.
	 */
	private String state;

	/**
	 * 国家.
	 */
	private String country;

	/**
	 * 安全问题.
	 */
	private String securityQuestion;

	/**
	 * 答案.
	 */
	private String answer;

	public UserExt() {
	}

	public UserExt(int id) {
		this.id = id;
	}

	public UserExt(int id, Integer level, String source, Date birthday,
			String zipCode, String address, String district, String city,
			String state, String country, String securityQuestion, String answer) {
		this.id = id;
		this.level = level;
		this.source = source;
		this.birthday = birthday;
		this.zipCode = zipCode;
		this.address = address;
		this.district = district;
		this.city = city;
		this.state = state;
		this.country = country;
		this.securityQuestion = securityQuestion;
		this.answer = answer;
	}

	/**  
	 * Get 用户ID.
	 * @return 用户ID
	 */
	@Id
	@Column(name = "ID", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	/**
	 * Set 用户ID.
	 * @param id 用户ID
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**  
	 * Get 用户等级.
	 * @return 用户等级
	 */

	@Column(name = "LEVEL")
	public Integer getLevel() {
		return this.level;
	}

	/**
	 * Set 用户等级.
	 * @param level 用户等级
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}

	/**  
	 * Get 用户来源.
	 * @return 用户来源
	 */

	@Column(name = "SOURCE", length = 100)
	public String getSource() {
		return this.source;
	}

	/**
	 * Set 用户来源.
	 * @param source 用户来源
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**  
	 * Get 生日.
	 * @return 生日
	 */

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "BIRTHDAY", length = 19)
	public Date getBirthday() {
		return this.birthday;
	}

	/**
	 * Set 生日.
	 * @param birthday 生日
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	/**  
	 * Get 邮政编码.
	 * @return 邮政编码
	 */

	@Column(name = "ZIP_CODE", length = 6)
	public String getZipCode() {
		return this.zipCode;
	}

	/**
	 * Set 邮政编码.
	 * @param zipCode 邮政编码
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**  
	 * Get 联系地址.
	 * @return 联系地址
	 */

	@Column(name = "ADDRESS", length = 200)
	public String getAddress() {
		return this.address;
	}

	/**
	 * Set 联系地址.
	 * @param address 联系地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**  
	 * Get 区.
	 * @return 区
	 */

	@Column(name = "DISTRICT", length = 50)
	public String getDistrict() {
		return this.district;
	}

	/**
	 * Set 区.
	 * @param district 区
	 */
	public void setDistrict(String district) {
		this.district = district;
	}

	/**  
	 * Get 城市.
	 * @return 城市
	 */

	@Column(name = "CITY", length = 50)
	public String getCity() {
		return this.city;
	}

	/**
	 * Set 城市.
	 * @param city 城市
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**  
	 * Get 省份.
	 * @return 省份
	 */

	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * Set 省份.
	 * @param state 省份
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**  
	 * Get 国家.
	 * @return 国家
	 */

	@Column(name = "COUNTRY", length = 50)
	public String getCountry() {
		return this.country;
	}

	/**
	 * Set 国家.
	 * @param country 国家
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**  
	 * Get 安全问题.
	 * @return 安全问题
	 */

	@Column(name = "SECURITY_QUESTION", length = 100)
	public String getSecurityQuestion() {
		return this.securityQuestion;
	}

	/**
	 * Set 安全问题.
	 * @param securityQuestion 安全问题
	 */
	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}

	/**  
	 * Get 答案.
	 * @return 答案
	 */

	@Column(name = "ANSWER", length = 100)
	public String getAnswer() {
		return this.answer;
	}

	/**
	 * Set 答案.
	 * @param answer 答案
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}

}
