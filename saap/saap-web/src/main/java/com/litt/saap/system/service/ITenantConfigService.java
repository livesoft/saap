package com.litt.saap.system.service;

import java.util.List;
import java.util.Map;

import com.litt.core.dao.page.IPageList;
import com.litt.core.dao.ql.PageParam;
import com.litt.saap.core.module.tenant.config.meta.TenantMetaAttrGroup;
import com.litt.saap.system.po.TenantConfig;
import com.litt.saap.system.vo.TenantConfVo;
import com.litt.saap.system.vo.TenantMemberVo;

/**
 * 
 * Tenant config service interface.
 * <pre><b>Description：</b>
 *    Tenant config Management
 * </pre>
 * 
 * @author <a href="mailto:littcai@hotmail.com">Bob.cai</a>
 * @since 2015-02-09
 * @version 1.0
 */
public interface ITenantConfigService
{ 

	/**
	 * init.
	 *
	 * @return the map
	 */
	public Map<Integer, TenantConfVo> init();
	
   	/**
	 * Save.
	 * @param tenantConfig TenantConfig
	 * @return id
	 */
	public Integer save(TenantConfig tenantConfig);
	
   	/**
	 * Update.
	 * @param tenantConfig TenantConfig
	 */
	public void update(TenantConfig tenantConfig);	
	
	/**
   * Update batch.
   *
   * @param tenantId the tenant id
   * @param metaAttrGroups the meta attr groups
   * @param paramMap the param map
   */
  public void updateBatch(int tenantId, TenantMetaAttrGroup[] metaAttrGroups, Map<String, String> paramMap);	
   
   	/**
	 * Delete by id.
	 * @param id id
	 */
	public void delete(Integer id);	
	
	/**
	 * Delete by instance.
	 * @param id id
	 */
	public void delete(TenantConfig tenantConfig);
	
	
	/**
	 * Delete by instance.
	 * @param id id
	 */
	public void deleteByTenantId(Integer tenantId);
	
	
	/**
	 * Batch delete by ids.
	 * @param ids ids
	 */
	public void deleteBatch(Integer[] ids);
	
	/**
	 * Load by id.
	 * @param id id
	 * @return TenantConfig
	 */
	public TenantConfig load(Integer id);	
	
	
	/**
	 * list by page.
	 * 
	 * @param pageParam params
	 * @return IPageList IPageList
	 */
	public IPageList listPage(PageParam pageParam);	

}