package com.litt.saap.system.po;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

import java.io.Serializable;

/**
 * <br>
 * Table:tenant_config<br>
 * @author Hibernate Tools 3.4.0.CR1
 * @version 1.0
 * @since Feb 5, 2015 3:23:00 PM
 */
@Entity
@org.hibernate.annotations.Entity(dynamicUpdate = true, dynamicInsert = true)
@Table(name = "tenant_config")
public class TenantConfig implements Serializable {
	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 序号.
	 */
	private Integer id;

	/**
	 * 租户ID.
	 */
	private int tenantId;

	/**
	 * 属性键.
	 */
	private String attrKey;

	/**
	 * 属性值.
	 */
	private String attrValue;

	public TenantConfig() {
	}

	public TenantConfig(int tenantId, String attrKey) {
		this.tenantId = tenantId;
		this.attrKey = attrKey;
	}

	public TenantConfig(int tenantId, String attrKey, String attrValue) {
		this.tenantId = tenantId;
		this.attrKey = attrKey;
		this.attrValue = attrValue;
	}

	/**  
	 * Get 序号.
	 * @return 序号
	 */
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	/**
	 * Set 序号.
	 * @param id 序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**  
	 * Get 租户ID.
	 * @return 租户ID
	 */

	@Column(name = "TENANT_ID", nullable = false)
	public int getTenantId() {
		return this.tenantId;
	}

	/**
	 * Set 租户ID.
	 * @param tenantId 租户ID
	 */
	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	/**  
	 * Get 属性键.
	 * @return 属性键
	 */

	@Column(name = "ATTR_KEY", nullable = false, length = 50)
	public String getAttrKey() {
		return this.attrKey;
	}

	/**
	 * Set 属性键.
	 * @param attrKey 属性键
	 */
	public void setAttrKey(String attrKey) {
		this.attrKey = attrKey;
	}

	/**  
	 * Get 属性值.
	 * @return 属性值
	 */

	@Column(name = "ATTR_VALUE", length = 500)
	public String getAttrValue() {
		return this.attrValue;
	}

	/**
	 * Set 属性值.
	 * @param attrValue 属性值
	 */
	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}

}
