Steps for build local environment.
=========================

# 本地开发：

1：创建数据库，执行saap-web/src/main/dbscripts/init中的建表和初始化脚本
2：执行saas-invoicing/src/main/dbscripts/init中的建表和初始化脚本
3：修改jdbc.properties的连接信息
4：启动程序

5：注册个人用户账号
6：订购一个空间（目前只有basic有效，且自动开通）

7：可能需要重新登录刷权限

